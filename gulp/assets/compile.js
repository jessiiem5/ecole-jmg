/**
 * Compiles cleaned, minified assets.
 */
module.exports = function (gulp, plugins) {

  // Dependencies.
  const runSequence = require('run-sequence');
  const usemin = require('gulp-usemin');

  return function (done) {
    runSequence('usemin', 'clean', function () {
      done();
    });
  };

};
