/**
 * Watches SASS files.
 */
module.exports = function (gulp, plugins) {

  // Global constants.
  const assets = require('../constants/assets');
  browserSync = require('browser-sync').create();

  return function () {

    // Activate Browsersync if "proxy" variable is defined
    if (plugins.util.env.proxy) {
      browserSync.init({
        proxy: plugins.util.env.proxy
      });
      // Else, activate livereload
    }else{
      plugins.livereload.listen();
    }

    gulp.watch(assets.path.resources + assets.path.sass, gulp.series('sass'));
  }

};
