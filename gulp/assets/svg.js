/**
 * Generates SVG files.
 */
module.exports = function (gulp, plugins) {

  // Dependencies.
  const runSequence = require('run-sequence');

  return function (done) {
    runSequence('icons', 'assets', 'pictos', 'concat-png', 'concat-svg', 'concat-fallback', 'clean', function () {
      done();
    });
  };

};