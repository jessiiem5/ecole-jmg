/**
 * Contains all constants related to assets.
 */
module.exports = {
  // Assets paths.
  path: {
    public: './wp-content/themes/onepager/assets', // Directory of where public assets are
    resources: './wp-content/themes/onepager/assets', // Directory of where raw assets are, could be the same as public, depending on framework
    usemin: './wp-content/themes/onepager/templates/includes/html-header.twig', // If used, directory containing files where assets are embedded
    css: '/css',
    scss: '/scss',
    sass: '/scss/**/*.scss',
    svg: '/svg/assets/*.svg',
    icons: '/svg/icons/*.svg',
    pictos: '/svg/pictos/*.svg',
    min: '/dist',
    dist: '/dist/output',
    tmp: '/dist/output/*tmp*',
    twig: '/dist/*.twig',
  },
};
