/**
 * Generates icons files.
 */
module.exports = function (gulp, plugins) {

  // Global constants.
  const assets = require('../constants/assets');

  // Dependencies.
  const gulpicon = require('gulpicon/tasks/gulpicon');
  const glob = require('glob');

  return gulpicon(
    glob.sync(assets.path.public + assets.path.icons),
    {
      enhanceSVG: true,
      pngfolder: 'icons',
      datasvgcss: 'icons.tmp.data.svg.css',
      datapngcss: 'icons.tmp.data.png.css',
      urlpngcss: 'icons.tmp.fallback.css',
      cssprefix: '.icon-',
      dest: assets.path.public + assets.path.dist,
    }
  );

};
