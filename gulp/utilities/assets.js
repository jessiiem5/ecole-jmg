/**
 * Generates SVG assets files.
 */
module.exports = function (gulp, plugins) {

  // Global constants.
  const assets = require('../constants/assets');

  // Dependencies.
  const gulpicon = require('gulpicon/tasks/gulpicon');
  const glob = require('glob');

  return gulpicon(
    glob.sync(assets.path.resources + assets.path.svg),
    {
      enhanceSVG: true,
      pngfolder: 'assets',
      tmpDir: 'test',
      datasvgcss: 'assets.tmp.data.svg.css',
      datapngcss: 'assets.tmp.data.png.css',
      urlpngcss: 'assets.tmp.fallback.css',
      cssprefix: '.asset-',
      dest: assets.path.public + assets.path.dist,
    }
  );

};
