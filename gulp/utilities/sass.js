/**
 * Main SASS handling.
 */
module.exports = function (gulp, plugins) {

  // Global constants.
  const assets = require('../constants/assets');
  const gulpif = require('gulp-if');
  const stripCssComments = require('gulp-strip-css-comments');

  // Define BrowserSync activation variable from "proxy" command variable
  let activateSync = false;
  if (plugins.util.env.proxy) {
    activateSync = true;
  }

  return function () {

    return gulp.src(assets.path.resources + assets.path.sass)

      // Prevent Crash.
      .pipe(plugins.plumber())

      // Filter out unchanged SCSS files.
      .pipe(plugins.cached('sass'))

      // Find files that depend on files that have changed.
      .pipe(plugins.sassInheritance({dir: assets.path.resources + assets.path.scss}))

      // Filter out internal imports (folders and files starting with "_").
      .pipe(plugins.filter(function (file) {
        return !/\/_/.test(file.path) || !/^_/.test(file.relative);
      }))

      // Compile.
      .pipe(plugins.sourcemaps.init())

      // Strip comments
      .pipe(stripCssComments())

      // Sass
      .pipe(plugins.sass())

      // Auto-prefix.
      .pipe(plugins.autoprefixer())

      // Write sourcemaps
      .pipe(plugins.sourcemaps.write('.', {includeContent: false}))

      // Output files
      .pipe(gulp.dest(assets.path.public + assets.path.css))

      // Activate Browsersync if "proxy" variable is defined
      .pipe(gulpif(
        // Condition
        activateSync,
        // IF true
        browserSync.stream(),
        // ELSE, activate LiveReload plugin
        plugins.livereload({start: true}))
      );
  };

};
