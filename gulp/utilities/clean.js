/**
 * Cleans unnecessary dist files.
 */
module.exports = function (gulp, plugins) {

  // Global constants.
  const assets = require('../constants/assets');

  // Dependencies.
  const del = require('del');

  return function (cb) {
    del([assets.path.public + assets.path.tmp, assets.path.public + assets.path.twig], cb);
  };

};
