let gulp = require('gulp');
let plugins = require('gulp-load-plugins')();

// Declare global variable for BrowserSync
let browserSync;

// Gulp usage.
gulp.task('help', require('./gulp/usage/help')(gulp, plugins));

// Assets processing.
gulp.task('compile', require('./gulp/assets/compile')(gulp, plugins));
gulp.task('svg', require('./gulp/assets/svg')(gulp, plugins));
gulp.task('watch', require('./gulp/assets/watch')(gulp, plugins));

// Utilities, do not call manually.
gulp.task('assets', require('./gulp/utilities/assets')(gulp, plugins));
gulp.task('clean', require('./gulp/utilities/clean')(gulp, plugins));
gulp.task('concat-fallback', require('./gulp/utilities/concat-fallback')(gulp, plugins));
gulp.task('concat-png', require('./gulp/utilities/concat-png')(gulp, plugins));
gulp.task('concat-svg', require('./gulp/utilities/concat-svg')(gulp, plugins));
gulp.task('icons', require('./gulp/utilities/icons')(gulp, plugins));
gulp.task('pictos', require('./gulp/utilities/pictos')(gulp, plugins));
gulp.task('sass', require('./gulp/utilities/sass')(gulp, plugins));
gulp.task('usemin', require('./gulp/utilities/usemin')(gulp, plugins));