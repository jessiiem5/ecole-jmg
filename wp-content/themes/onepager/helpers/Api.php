<?php

class Api
{

    public function __construct()
    {
    }

    public static function init()
    {
        add_action('rest_api_init', function () {
            register_rest_route('wp/v2', '/equipe/(?P<id>\d+)', array(
                'methods' => 'GET',
                'callback' => __CLASS__ . '::api_load_member',
            ));
        });
    }

    public static function api_load_member($request)
    {

        $member = get_post($request['id']);
        // hehe.. long members (☞ﾟ∀ﾟ)☞
        $long_members = get_posts(array(
                'post_type' => 'equipe',
                'post_status' => 'publish',
                'orderby' => 'date',
                'order' => 'DESC',
                'fields' => 'ids', // Only get post IDs
                'posts_per_page' => -1
            )
        );
        // Get the current index in the array of long article IDs
        $current_index = array_search($member->ID, $long_members);
        $last_index = count($long_members);


        // Get the next post if it exists
        if (isset($long_members[$current_index + 1])) {
            $nextPost = get_post($long_members[$current_index + 1]);
        } else {
            $nextPost = get_post($long_members[0]);
        }



        // Get the previous post if it exists
        if (isset($long_members[$current_index - 1])) {
            $previousPost = get_post($long_members[$current_index - 1]);
        } else {
            $previousPost = get_post($long_members[$last_index - 1]);
        }

        $return = '<div class="photo"
                 style="background-image:url(' . get_the_post_thumbnail_url($member->ID, 'full') . ')">
<h2>' . get_field('team_phrase', $member->ID) . '</h2>
<a class="lien-membre-precedent desktop-show mobile-hide member" data-id="'. $previousPost->ID . '">Voir le pro précédent</a>
</div>
<div class="text">
    <h2>' . $member->post_title . '</h2>
    <h3>' . get_field('team_title', $member->ID) . '</h3>
    <div class="programmes">
        <p class="title">Programmes en charge:</p>
        <p class="text">' . get_field('team_charge', $member->ID) . '</p>
    </div>
    <div class="experience">
        <p class="title">Expérience:</p>
        <p class="text">' . get_field('team_experience', $member->ID) . '</p>
    </div>
    <div class="valeur">
        <p class="title">Valeur:</p>
        <p class="text">' . get_field('team_value', $member->ID) . '</p>
    </div>
    <div class="raquette">
        <p class="title">Raquette:</p>
        <p class="text">' . get_field('team_racket', $member->ID)
            .
            '</p>
    </div>
    <div class="description">
        <p class="text">' . $member->post_content . '</p>
    </div>
    <div class="certifications">
        <p class="title">Certifications:</p>
        <p class="text">' . get_field('team_certifications', $member->ID) . '</p>
    </div>';
        if ($member->lien_cours) {
            $return .= '<a class="lien-cours" href="' . $member->lien_cours['url'] . '">Réserver des cours</a>';
        }
        $return .= '
        <a class="lien-membre-precedent desktop-hide mobile-show member" data-id="'. $previousPost->ID . '">Voir le pro précédent</a>
        <a class="lien-membre-suivant member" data-id="'. $nextPost->ID . '">Voir le pro suivant</a></div>';

        return $return;
    }


}
