<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package onepager
 */

get_header(); ?>

<section id="slider-default" style="background-image:url('<?php echo get_field('image_de_fond')['url']; ?>')">
    <div class="inner-slider-cours">
        <h1><?php the_field('titre'); ?></h1>
        <p class="subtitle"><?php the_field('sous-titre'); ?></p>
    </div>
</section>

<section id="page">
    <div class="container">
        <div class="row">

            <?php while (have_posts()) : the_post(); ?>

                <?php the_content(); ?>

            <?php endwhile; // end of the loop. ?>

        </div><!-- #main -->
    </div><!-- #primary -->
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
