<?php
/*
Template Name: ligue
*/


get_header;

// Selection de l'activité en fonction du slug de la page

global $post;
$activite = get_post( $post )->post_name;
($activite =="cours-de-tennis") ? $activite="tennis" : $activite="camps";

?>

    <section class="zone1">

        <div class="container-fluid fond_blanc" role="main">

            <div class="row no-margin">


                <div class="row no-margin">

                    <div class="col-xs-12 col-sm-4">

                        <div class="row">


                        </div>

                    </div>


                    <div class="col-xs-12  col-sm-8   camp-dete no-padding">





                        <div>


                            <?php

                            $image = get_field('photo');
                            $postid = get_the_ID();
                            $activite = get_field('type', $postid);
                            $public = get_field('public_cible', $postid);

                            ?>

                            <div class="img_cours" style="background-image: url('<?php echo $image['url'] ?>')"></div>

                            <div class="row info-camps no-margin">

                                <p class="titre-camps">Commentaires de la semaine</p>

                                <p>Ce camp d’été est offert à la semaine pour les enfants de 6 à 10 ans. Le Camp Junior permet à votre enfant de s’initier au sport, ou encore de perfectionner des talents déjà développés par l’entremise d’activités interactives et amusantes. Tous les niveaux sont la bienvenue. Un séjour au sein de notre camp permet par ailleurs de développer l’autonomie de votre enfant et facilite le partage de valeurs saines avec un réseau de nouveaux amis, tout en jouant à un sport magnifique : le tennis!</p>


                            </div>
                    </div>



                </div>

            </div>

    </section>

<?php get_sidebar( "infolettre" ); ?>

<?php get_footer(); ?>