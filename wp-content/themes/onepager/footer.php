<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package onepager
 */
?>

<footer>
    <div class="col-xs-12 col-sm-3 col-logo">
        <img src="<?php the_field('footer_logo', 'option'); ?>"/>
        <?php the_field('footer_adresse','option'); ?>
    </div>
    <div class="col-xs-12 col-sm-9 col-menu">
        <div class="row">
            <div class="col-xs-12 col-sm-3"><?php wp_nav_menu(array('theme_location' => 'footer1', 'container' => false, 'menu_class' => '')); ?></div>
            <div class="col-xs-12 col-sm-3"><?php wp_nav_menu(array('theme_location' => 'footer2', 'container' => false, 'menu_class' => '')); ?></div>
            <div class="col-xs-12 col-sm-3"><?php wp_nav_menu(array('theme_location' => 'footer3', 'container' => false, 'menu_class' => '')); ?></div>
            <div class="col-xs-12 col-sm-3"><?php wp_nav_menu(array('theme_location' => 'footer4', 'container' => false, 'menu_class' => '')); ?></div>
        </div>
    </div>


</footer>

<?php wp_footer(); ?>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

</body>
</html>
