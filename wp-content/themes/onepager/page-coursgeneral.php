<?php
/*
Template Name: cours général
*/

get_header('section');

?>

<section id="infos-generales">
    <div class="container">

        <div class="row">
            <div class="col-xs-12">
                <h2 class="titre-camps">CAMPS D'ÉTÉ 2021</h2>
            </div>

            <?php if (have_rows('informations_gererales')): while (have_rows('informations_gererales')): the_row(); ?>

            <div class="col-sm-7">
                <?php the_sub_field('section_de_gauche_informations'); ?>
            </div>

            <div class="col-sm-4 col-sm-offset-1 margin-mobile">

                <?php the_sub_field('section_de_droite_informations'); ?>
            </div>
            <?php endwhile; endif; ?>
        </div>

    </div>

    <div class="overflow-hidden">
        <img class="balle" src="<?php echo get_template_directory_uri(); ?>/images/balle-tennis.png" alt="balle de tennis">
    </div>
</section>


<?php if (have_rows('fonctionnement_des_camps')) : ?>
    <?php while (have_rows('fonctionnement_des_camps')): the_row(); ?>
    <?php $imgFonctionnement = get_sub_field('image_fond_fonctionnement'); ?>
<section id="fonctionnement" style="background: linear-gradient( rgba(255, 255, 255, 0.9), rgba(255, 255, 255, 0.9) ), url(<?= $imgFonctionnement; ?>); background-size: cover; background-position: center; background-repeat: no-repeat;">
    <?php endwhile; ?>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="titre-camps"><?php the_field('titre_fonctionnement_des_camps'); ?></h2>
            </div>

            <?php while (have_rows('fonctionnement_des_camps')): the_row(); ?>

            <div class="col-sm-7">
                <?php the_sub_field('section_de_gauche_fonctionnement'); ?>
            </div>

            <div class="col-sm-4 col-sm-offset-1 margin-mobile">
                <?php the_sub_field('section_de_droite_fonctionnement'); ?>
            </div>

            <?php endwhile; ?>
        </div>

    </div>

</section>
<?php endif; ?>



<section id="galerie">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="titre-camps"><?php the_field('titre_section_galerie_evenement'); ?></h2>
            </div>
            <div class="col-xs-12">
                <?php echo do_shortcode(get_field('galerie_photo')); ?>
            </div>

        </div>
    </div>
</section>

<?php get_sidebar("infolettre"); ?>

<?php get_footer(); ?>

<script type="text/javascript">
    $(document).ready(function(){

        // PUISQUE CONFUSION ENTRE FANCYBOX ET LIGHTBOX, JE SUPPRIME TOUT CE QUI EST ID "FANCYBOX"
        $("[id^='fancybox']").remove();




        // Pour déplacer les balles de tennis

        $(window).scroll(function(){

            function isScrolledIntoView(elem)
            {
                var docViewTop = $(window).scrollTop();
                var docViewBottom = docViewTop + $(window).height();

                var elemTop = $(elem).offset().top;
                var elemBottom = elemTop + $(elem).height();

                return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
            }
            function Utils() {}

            Utils.prototype = {
                constructor: Utils,
                isElementInView: function (element, fullyInView) {
                    var pageTop = $(window).scrollTop();
                    var pageBottom = pageTop + $(window).height();
                    var elementTop = $(element).offset().top;
                    var elementBottom = elementTop + $(element).height();

                    if (fullyInView === true) {
                        return ((pageTop < elementTop) && (pageBottom > elementBottom));
                    } else {
                        return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
                    }
                }
            };
            var Utils = new Utils();
            var balleInfos = Utils.isElementInView($('#infos-generales .balle'), false);
            var balleInscription = Utils.isElementInView($('#inscription-achat .balle'), false);
            var s = $(this).scrollTop();

            if (balleInfos) {
                // $('#infos-generales .balle').css({
                //     left:  s +'px',
                //     transform: 'rotate(45deg)',
                //     transition: 'all ' + .1 +'s ease'
                // });

                $('#infos-generales .balle').addClass('animate');

            }

            if (balleInscription) {
                $('#inscription-achat .balle').addClass('animate');

            }


        });

    });
</script>
<script>
  fbq('track', 'Purchase');
</script>
