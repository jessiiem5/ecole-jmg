$(document).ready(function () {

    $('.slider-equipe').slick({
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.slider-temoignages').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    var owl = $('.owl-camps');
    owl.owlCarousel({
        items: 4,
        loop: true,
        autoplay: true,
        autoplayHoverPause: true,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3

            },
            1300: {
                items: 4

            }

        }
    });

    $('body').on('click', '.navbar-toggle', function () {
        $('.mega').toggle('slow');
        $icon = $('.icon-bar');
        if ($icon.hasClass('is-opened')) {
            $icon.addClass('is-closed').removeClass('is-opened');
            $('.icon-bar-texte').fadeIn();
        } else {
            $icon.removeClass('is-closed').addClass('is-opened');
            $('.icon-bar-texte').fadeOut();
        }
    });


    // Get the modal
    var modal = document.getElementById("myModal");

    onclickListeners();


// Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    if (span != undefined) {
        // When the user clicks on <span> (x), close the modal
        span.onclick = function () {
            modal.style.display = "none";
        }
    }


// When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    function onclickListeners() {
        // Get the button that opens the modal
        var btns = document.getElementsByClassName("member");

        Array.prototype.forEach.call(btns, member => {

            var id = member.dataset.id;
            member.onclick = function () {
                $(".inner").css("cursor", "progress");
                $.ajax({
                    url: '/wp-json/wp/v2/equipe/' + id,
                    method: 'GET',
                    contentType: 'application/json',
                    beforeSend: function (xhr) {
                    },
                    success: function (data) {
                        document.querySelector('.inner-modal').innerHTML = data;
                        modal.style.display = "block";
                        onclickListeners();
                        $(".inner").css("cursor", "default");
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
        });
    }

    // Mobile bavigation   ------------------
    let nb = $(".nav-button"), nh = $("#menu-principal"), nsh = $("#menu-principal li.menu-item-has-children");
    $(window).load(function () {
        // Run code

        nsh.on("click", function () {
            if ($(this).hasClass("vis-m")) showSubMenu($(this)); else hideSubMenu($(this));
        });

        if ($(window).width() < 900) {
            nh.slideUp(500);
            nsh.find('.sub-menu').slideUp(500);
            nsh.addClass('vis-m');
        }
    });
    $(window).resize(function () {

        if ($(window).width() < 900) {
            nh.slideUp(500);
            nsh.find('.sub-menu').slideUp(500);
            nsh.addClass('vis-m');
        }
    });

    function showMenu() {
        nb.removeClass("vis-m");
        nh.addClass('open');
        nh.slideDown(500, function () {
            nh.find('li').css('border-top-color', '#fff');
        });
    }

    function hideMenu() {
        nh.find('li').css('border-top-color', '#000');
        nb.addClass("vis-m");
        nh.removeClass('open');
        nh.slideUp(500);
    }

    function showSubMenu(e) {
        nsh.addClass('vis-m');
        nsh.find('.sub-menu').hide();
        e.removeClass("vis-m");
        e.find('.sub-menu').show();
    }

    function hideSubMenu(e) {
        nsh.addClass('vis-m');
        e.addClass("vis-m");
        e.find('.sub-menu').hide();
    }

    nb.on("click", function () {
        if ($(this).hasClass("vis-m")) showMenu(); else hideMenu();
    });

    var acc = document.getElementsByClassName("accordion-header");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            /* Toggle between adding and removing the "active" class,
            to highlight the button that controls the panel */
            this.classList.toggle("active");

            /* Toggle between hiding and showing the active panel */
            if ($(window).width() < 768) {
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight === "800px") {
                    panel.style.maxHeight = "0";
                    panel.style.padding = "0 20px";
                } else {
                    panel.style.maxHeight = "800px";
                    panel.style.padding = "10px 20px";
                }
            }
            else {
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight === "500px") {
                    panel.style.maxHeight = "0";
                    panel.style.padding = "0 60px";
                } else {
                    panel.style.maxHeight = "500px";
                    panel.style.padding = "30px 60px";
                }
            }
        });
    }




});
