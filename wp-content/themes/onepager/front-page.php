<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package onepager
 */

get_header(); ?>

<section id="slider-home" style="background-image:url('<?php the_field('home_image_header'); ?>')">
    <div class="inner-slider-home">
        <h1><?php the_field('home_header_text'); ?></h1>
        <?php $btn = get_field('home_header_button');
        if ($btn != null) { ?>
            <a class="btn theme grand"
               href="<?php echo $btn['url']; ?>" <?php echo $btn['target'] != "" ? 'target="_blank"' : '' ?>><?php echo $btn['title']; ?></a>
        <?php } ?>
    </div>
</section>
<section id="cours">
    <div class="container" role="main">

        <?php if (have_rows('cours')) : ?>
            <div class="cours-container">
                <?php while (have_rows('cours')) : the_row(); ?>

                    <div class="cours">
                        <?php $lien = get_sub_field('lien_vers_la_page');
                        $imgFondCours = get_sub_field('image_du_cours');
                        if ($lien != null) {
                            ?>
                            <a href="<?php the_sub_field('lien_vers_la_page'); ?>"
                               style="background: linear-gradient( 180deg,#31313100 0%,#31313100 30%, #313131 70% ), url(<?= $imgFondCours; ?>); background-size: cover; background-repeat: no-repeat; background-position: center;">
                                <h3><?php the_sub_field('nom_du_cours'); ?></h3>
                                <div class="btn-cours"><span>En savoir plus</span></div>
                            </a>
                            <?php
                        } else {
                            ?>
                            <div class="a"
                                 style="background: linear-gradient( 180deg,#31313100 0%,#31313100 30%, #313131 70% ), url(<?= $imgFondCours; ?>); background-size: cover; background-repeat: no-repeat; background-position: center;">
                                <h3><?php the_sub_field('nom_du_cours'); ?></h3>
                                <div class="btn-cours"><span>À venir</span></div>
                            </div>
                            <?php
                        }
                        ?>


                    </div>

                <?php endwhile ?>
            </div>
        <?php endif ?>

    </div>
</section>


<?php $imgSectionInscription = get_field('image_bandeau_blanc'); ?>
<section id="inscription"
         style="background: linear-gradient( rgba(255, 255, 255, 1) 0%, rgba(255, 255, 255, 0.79) 20%, rgba(255, 255, 255, 0.79) 80%,rgba(255, 255, 255, 1) 100%), url(<?= $imgSectionInscription; ?>);background-size: cover; background-repeat: no-repeat; background-position: center;">
    <div class="container">
        <div class="row">

            <div class="text-center">
                <?php the_field('contenu_bandeau_blanc'); ?>

                <?php $btnBandeau = get_field('home_bandeau_blanc_button');
                if ($btnBandeau != null) { ?>
                    <a class="btn theme grand"
                       href="<?php echo $btnBandeau['url']; ?>" <?php echo $btnBandeau['target'] != "" ? 'target="_blank"' : '' ?>><?php echo $btnBandeau['title']; ?></a>
                <?php } ?>
            </div>

        </div>
    </div>
</section>

<section id="cours" class="second-section">
    <div class="container" role="main">
        <div class="row">

            <?php if (have_rows('other_cards_section')) : ?>
                <div class="cours-container">
                    <?php while (have_rows('other_cards_section')) : the_row(); ?>

                        <div class="cours">

                            <?php $lien = get_sub_field('card_link');
                            $imgFondCours = get_sub_field('card_image');
                            if ($lien != null) {
                                ?>
                                <a href="<?php the_sub_field('card_link'); ?>"
                                   style="background: linear-gradient( 180deg,#31313100 0%,#31313100 30%, #313131 70% ), url(<?= $imgFondCours; ?>); background-size: cover; background-repeat: no-repeat; background-position: center;">
                                    <h3><?php the_sub_field('card_text'); ?></h3>
                                    <div class="btn-cours"><span>En savoir plus</span></div>
                                </a>
                                <?php
                            } else {
                                ?>
                                <div class="a"
                                     style="background: linear-gradient( 180deg,#31313100 0%,#31313100 30%, #313131 70% ), url(<?= $imgFondCours; ?>); background-size: cover; background-repeat: no-repeat; background-position: center;">
                                    <h3><?php the_sub_field('card_text'); ?></h3>
                                    <div class="btn-cours"><span>À venir</span></div>
                                </div>
                                <?php
                            }
                            ?>

                        </div>

                    <?php endwhile; ?>
                </div>
            <?php endif; ?>

        </div>
    </div>
</section>

<?php $imgFondBandeauNoir = get_field('home_image_black'); ?>
<section id="bandeau-noir" style="
        background-blend-mode: saturation;background: linear-gradient(black, black), url(<?= $imgFondBandeauNoir; ?>); background-size: cover; background-repeat: no-repeat; background-position: center; background-attachment: fixed">
    <div class="container">
        <div class="row">

            <div class="text-center">
                <?php $btnBandeau = get_field('field_home_button_black'); ?>
                <a class="btn theme grand"
                   href="<?php echo $btnBandeau['url']; ?>" <?php echo $btnBandeau['target'] != "" ? 'target="_blank"' : '' ?>><?php echo $btnBandeau['title']; ?></a>
                <?php the_field('home_text_black'); ?>
            </div>


        </div>
    </div>
</section>

<section id="carousel-team">
    <div class="phrase-background">
        <?php the_field('home_phrase_background'); ?>
    </div>
    <div class="slider slider-equipe">
        <?php $team = get_posts(array(
            'numberposts' => -1,
            'post_type' => 'equipe')); ?>

        <?php foreach ($team as $teamMember) {
            ?>
            <div class="slide member"
                 style="background-image: url('<?php echo get_the_post_thumbnail_url($teamMember->ID, 'full'); ?>')" data-id="<?php echo $teamMember->ID; ?>">
                <h3 class="name"><?php echo $teamMember->post_title ?></h3>
            </div>
            <?php
        } ?>
    </div>
    <div class="text-center">
        <?php the_field('home_text_team'); ?>
        <?php $btnEquipe = get_field('home_button_team');
        if ($btnEquipe != null) { ?>
            <a class="btn theme grand"
               href="<?php echo $btnEquipe['url']; ?>" <?php echo $btnEquipe['target'] != "" ? 'target="_blank"' : '' ?>><?php echo $btnEquipe['title']; ?></a><?php } ?>
    </div>
</section>
<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">Fermer<img
                    src="<?php echo get_home_url(); ?>/wp-content/themes/onepager/assets/img/icon.x.svg"/></span>
        <div class="inner-modal">

            <div class="photo">
                <h2></h2>
                <a class="lien-membre-precedent desktop-show mobile-hide member" data-id=""></a>
            </div>
            <div class="text">
                <h2></h2>
                <h3></h3>

                <a class="lien-membre-precedent desktop-hide mobile-show member" data-id=""></a>
                <a class="lien-membre-suivant member" data-id=""></a></div>

        </div>
    </div>

</div>
<?php get_sidebar("infolettre"); ?>


<?php get_footer(); ?>
