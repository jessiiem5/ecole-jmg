<?php

include_once(dirname(__FILE__) . '/fields/home-fields.php');
include_once(dirname(__FILE__) . '/fields/option-fields.php');
include_once(dirname(__FILE__) . '/fields/team-fields.php');
include_once(dirname(__FILE__) . '/fields/cours-fields.php');
include_once(dirname(__FILE__) . '/fields/generic-fields.php');
include_once(dirname(__FILE__) . '/fields/cours-juniors-fields.php');
include_once(dirname(__FILE__) . '/fields/cours-post-type-fields.php');
require_once(dirname(__FILE__) . '/helpers/Api.php');

add_action('init', ['Api', 'init']);

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Paramètres général',
        'menu_title'	=> 'Paramètres général',
        'menu_slug' 	=> 'general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

}

register_nav_menus( array(
	'top' => 'Navigation principale',
	'mega' => 'Mega menu',
    'footer1' => 'Footer - Colonne 1',
    'footer2' => 'Footer - Colonne 2',
    'footer3' => 'Footer - Colonne 3',
    'footer4' => 'Footer - Colonne 4',
));


//Page Slug Body Class

function add_slug_body_class( $classes ) {
global $post;
if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );





function wpb_adding_scripts()
{
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js', array(), null, false );

	wp_register_script( 'isotope', get_template_directory_uri() . '/js/isotope.js', array('jquery'), '1.1', true );
	wp_enqueue_script('isotope');

	wp_register_script( 'imagesloaded', get_template_directory_uri() . '/js/imagesloaded.js', array('jquery'), '1.1', true);
	wp_enqueue_script('imagesloaded');


	wp_register_script('bootstrap_script', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '1.1', true);
	wp_enqueue_script('bootstrap_script');

    wp_register_script('owl_script', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1.1', true);
    wp_enqueue_script('owl_script');

/*	wp_register_script('flexslider_script', get_template_directory_uri() . '/js/jquery.flexslider.js', array('jquery'), '1.1', true);
	wp_enqueue_script('flexslider_script');*/

	wp_register_script('colorbox_script', get_template_directory_uri() . '/js/jquery.colorbox-min.js', array('jquery'), '1.1', true);
	wp_enqueue_script('colorbox_script');

}


add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' );


function wpb_adding_styles()
{

	wp_register_style('bootstrap_style', get_template_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style('bootstrap_style');

	wp_register_style('bootstrap_social_style', get_template_directory_uri() . '/css/bootstrap-social.css');
	wp_enqueue_style('bootstrap_social_style');

	wp_register_style('font_awesome_style', get_template_directory_uri() . '/css/font-awesome.css');
	wp_enqueue_style('font_awesome_style');

	wp_register_style('base_style', get_template_directory_uri() . '/css/style_update.css');
	wp_enqueue_style('base_style');

    wp_register_style('owl_style', get_template_directory_uri() . '/css/owl.carousel.css');
    wp_enqueue_style('owl_style');

    wp_register_style('owl_style_transition', get_template_directory_uri() . '/css/owl.transitions.css');
    wp_enqueue_style('owl_style_transition');

	/*wp_register_style('colorbox_style', get_template_directory_uri() . '/css/colorbox.css');
	wp_enqueue_style('colorbox_style');*/

	wp_register_style('bootstrap_theme_style', get_template_directory_uri() . '/css/bootstrap-theme.css');
	wp_enqueue_style('bootstrap_theme_style');

    wp_register_style('2021_style', get_template_directory_uri() . '/assets/css/global.css');
    wp_enqueue_style('2021_style');

}


add_action( 'wp_enqueue_scripts', 'wpb_adding_styles' );


class Menu_With_Description extends Walker_Nav_Menu {
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

		$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '<br /><span class="sub">' . $item->description . '</span>';
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}

// Post type cours

add_action('init', 'cours_init');

function cours_init()
{
	$labels = array(
		'name' => 'Cours',
		'singular_name' => 'Cours',
		'add_new' => 'Ajouter un cours',
		'add_new_item' => 'Ajouter un nouveau cours',
		'edit_item' => 'Modifier un nouveau cours',
		'new_item' => 'Nouveau cours',
		'view_item' => 'Voir le cours',
		'search_items' => 'Rechercher un cours',
		'not_found' =>  'Aucune cours trouvé',
		'not_found_in_trash' => 'Aucun cours dans la poubelle',
		'menu_icon' => 'dashicons-format-status',
		'parent_item_colon' => '',
		'menu_name' => 'Cours'

	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 3,
		'supports' => array('title', 'editor', 'thumbnail')
	);
	register_post_type('cours',$args);

	$labels = array(
		// Le nom au pluriel
		'name'                => _x( 'Galerie', 'Post Type General Name'),
		// Le nom au singulier
		'singular_name'       => _x( 'Galerie', 'Post Type Singular Name'),
		// Le libellé affiché dans le menu
		'menu_name'           => __( 'galerie'),
		// Les différents libellés de l'administration
		'all_items'           => __( 'Toutes les images'),
		'view_item'           => __( 'Voir les images'),
		'add_new_item'        => __( 'Ajouter une nouvelle image'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer les images'),
		'update_item'         => __( 'Modifier les images'),
		'search_items'        => __( 'Rechercher une image'),
		'not_found'           => __( 'Non trouvé'),
		'not_found_in_trash'  => __( 'Non trouvé dans la corbeille'),
	);

	// On peut définir ici d'autres options pour notre custom post type

	$args = array(
		'label'               => __( 'galerie'),
		'description'         => __( 'Tous sur galerie'),
		'labels'              => $labels,
		// On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		/*
		* Différentes options supplémentaires
		*/
		'hierarchical'        => false,
		'public'              => true,
		'has_archive'         => true,
	);

  register_post_type( 'galerie', $args );
}

// Post type cours

add_action('init', 'team_init');

function team_init()
{
    $labelsT = array(
        'name' => 'Équipe',
        'singular_name' => 'Équipe',
        'add_new' => 'Ajouter un membre de l\'équipe',
        'add_new_item' => 'Ajouter un membre de l\'équipe',
        'edit_item' => 'Modifier un membre de l\'équipe',
        'new_item' => 'Nouveau membre',
        'view_item' => 'Voir le membre de l\'équipe',
        'search_items' => 'Rechercher un membre de l\'équipe',
        'not_found' =>  'Aucune membre trouvé',
        'not_found_in_trash' => 'Aucun membre dans la poubelle',
        'menu_icon' => 'dashicons-nametag',
        'parent_item_colon' => '',
        'menu_name' => 'Équipe'

    );
    $args = array(
        'labels' => $labelsT,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 3,
        'supports' => array('title', 'editor', 'thumbnail')
    );
    register_post_type('equipe',$args);
}

function custom_taxonomy() {

    register_taxonomy(
        'custom_taxonomy_galerie',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'galerie',        //post type name
        array(
            'hierarchical' => true,
            'label' => 'Categorie Galerie',  //Display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'themes', // This controls the base slug that will display before each term
                'with_front' => false // Don't display the category base before
            )
        )
    );
}

add_action( 'init', 'custom_taxonomy');


function add_js_scripts() {
	wp_enqueue_script( 'script', get_template_directory_uri().'/js/script.js', array('jquery'), '1.0', true );

	// pass Ajax Url to script.js
	wp_localize_script('script', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
}
add_action('wp_enqueue_scripts', 'add_js_scripts');



add_action( 'wp_ajax_mon_action', 'mon_action' );
add_action( 'wp_ajax_nopriv_mon_action', 'mon_action' );

function mon_action() {

	$id = $_POST['id'];


	$args = array(
		'numberposts' => -1,
		'post_type' => 'cours',
		'p' => $id
	);

	$ajax_query = new WP_Query($args);


	if ( $ajax_query->have_posts() ) : while ( $ajax_query->have_posts() ) : $ajax_query->the_post();
		get_template_part( 'article' );
	endwhile;
	endif;

	die();
}


// Ajouter des colonnes au custom post type cours

function wpc_colonne($columns) {
	return array_merge( $columns, array('type' => 'Activité', 'public_cible' => 'Public cible') );
}
add_filter('manage_cours_posts_columns' , 'wpc_colonne',10,2);


function data_colonne($name) {

	global $post;

	switch ($name) {
		case 'type':
			echo get_field('type');
			break;
		case 'public_cible':
			echo get_field('public_cible');
			break;
	}
}

add_action('manage_cours_posts_custom_column', 'data_colonne',10,2);



// Retirer une colonne par défaut

add_filter('manage_cours_posts_columns', 'columns_remove_date');

function columns_remove_date($defaults) {
	// to get defaults column names:
	 print_r($defaults);
	unset($defaults['date']);
	return $defaults;
}





function my_column_register_sortable( $columns )
{
	$columns['type'] = 'type';
	return $columns;
}

add_filter("manage_edit-cours_sortable_columns", "my_column_register_sortable" );


function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );





function bootstrap_pagination($pages = '', $range = 2)
{
	$showitems = ($range * 2)+1;

	global $paged;
	if(empty($paged)) $paged = 1;

	if($pages == '')
	{
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if(!$pages)
		{
			$pages = 1;
		}
	}

	if(1 != $pages)
	{
		echo "<div class='pagination pagination-centered'><ul>";
		if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."'>&laquo;</a></li>";
		if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a></li>";

		for ($i=1; $i <= $pages; $i++)
		{
			if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
			{
				echo ($paged == $i)? "<li class='active'><span class='current'>".$i."</span></li>":"<li><a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a></li>";
			}
		}

		if ($paged < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a></li>";
		if ($paged < $pages-1 && $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."'>&raquo;</a></li>";
		echo "</ul></div>\n";
	}
}
