<?php
/*
Template Name: cours 2021
*/


get_header();

// Selection de l'activité en fonction du slug de la page

global $post;
$activite = get_post($post)->post_name;
($activite == "cours-de-tennis") ? $activite = "tennis" : $activite = "camps";

?>
    <section id="slider-cours" style="background-image:url('<?php echo get_field('image_de_fond')['url']; ?>')">
        <div class="inner-slider-cours">
            <h1><?php the_field('titre'); ?></h1>
            <p class="subtitle"><?php the_field('sous-titre'); ?></p>
        </div>
    </section>

    <section id="cours-intro">
        <div class="phrase-background">
            <?php the_field('page_cours_texte_background'); ?>
        </div>
        <div class="container">
            <div class="row">
                <div class="text-center col-text">
                    <?php the_field('page_cours_texte_intro'); ?>
                </div>
            </div>
        </div>
    </section>

    <section id="cours-pourquoi">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center col">
                    <h2><?php the_field('page_cours_pourquoi_titre'); ?></h2>
                </div>
                <?php $cols = get_field('page_cours_pourquoi_colonnes'); ?>
                <?php foreach ($cols as $col) {
                    ?>
                    <div class="col-lg-4 col-12 text-center col">
                        <img src="<?php echo $col['icone']; ?>"/>
                        <?php echo $col['texte']; ?>

                    </div>
                <?php }
                ?>
                <div class="text-center">
                    <?php $btnPourquoi = get_field('page_cours_pourquoi_bouton'); ?>
                    <a class="btn theme grand"
                       href="<?php echo $btnPourquoi['url']; ?>" <?php echo $btnPourquoi['target'] != "" ? 'target="_blank"' : '' ?>><?php echo $btnPourquoi['title']; ?></a>
                </div>
            </div>
        </div>
    </section>

    <section id="cours-mission">
        <div class="col">
            <div class="col-lg-6 col-12 image">
                <img src="<?php the_field('page_cours_mission_image'); ?>"/>
            </div>
            <div class="col-lg-6 col-12 texte">
                <?php the_field('page_cours_mission_texte'); ?>
            </div>
        </div>
    </section>
<?php
$col = 2;
$temoignages = get_field('page_cours_temoignages');
if($temoignages)
{
    $len = count($temoignages);

?>
    <section id="cours-temoignages">
        <div class="col-12 text-center col">
            <h2><?php the_field('page_cours_temoignages_titre'); ?></h2>
        </div>
        <div class="slider slider-temoignages">


            <?php foreach ($temoignages as $index => $temoignage) {

                if ($index % $col == 0) {
                    ?>
                    <div class="slide">
                    <div class="inner-slide">
                    <?php
                }
                ?>
                <div class="temoignage">
                    <?php echo $temoignage['texte'] ?>
                </div>
                <?php
                if ($index % $col == $col - 1 || $index == $len - 1) {
                    ?>
                    </div>
                    </div>
                    <?php
                }
                ?>
                <?php
            } ?>
        </div>
    </section>
    <?php }
else
{
    ?>
    <div style="height:180px;background:#efefef;"></div>
    <?php
}?>

    <section id="cours-infos">
        <div class="col">
            <div class="col-lg-6 col-12 texte">
                <h2><?php the_field('page_cours_info_titre'); ?></h2>
                <div class="grid">
                    <div class="left">
                        <img src="<?php echo get_home_url(); ?>/wp-content/themes/onepager/assets/img/icon.couts.svg"/>
                        <h3>Coûts:</h3>
                    </div>
                    <div class="right">
                        <p><?php the_field('page_cours_info_couts'); ?></p>
                    </div>
                    <div class="left">
                        <img src="<?php echo get_home_url(); ?>/wp-content/themes/onepager/assets/img/icon.materiel.svg"/>
                        <h3>Matériel requis:</h3>
                    </div>
                    <div class="right">
                        <p><?php the_field('page_cours_info_materiel'); ?></p>
                    </div>
                    <div class="left">
                        <img src="<?php echo get_home_url(); ?>/wp-content/themes/onepager/assets/img/icon.journee.svg"/>
                        <h3>Journée typique:</h3>
                    </div>
                    <div class="right">
                        <p><?php the_field('page_cours_info_journee'); ?></p>
                    </div>
                    <div class="left">
                        <img src="<?php echo get_home_url(); ?>/wp-content/themes/onepager/assets/img/icon.responsables.svg"/>
                        <h3>Responsables:</h3>
                    </div>
                    <div class="right">
                        <p><?php the_field('page_cours_info_responsables'); ?></p>
                    </div>
                </div>
                <?php $btnInfos = get_field('page_cours_info_bouton'); ?>
                <a class="btn theme grand"
                   href="<?php echo $btnInfos['url']; ?>" <?php echo $btnInfos['target'] != "" ? 'target="_blank"' : '' ?>><?php echo $btnInfos['title']; ?></a>
            </div>
            <div class="col-lg-6 col-12 image">
                <img src="<?php the_field('page_cours_info_image'); ?>"/>
            </div>
        </div>
    </section>

    <section id="cours-sites">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <?php $btnSites = get_field('page_cours_site_lien'); ?>
                    <a class="btn theme grand"
                       href="<?php echo $btnSites['url']; ?>" <?php echo $btnSites['target'] != "" ? 'target="_blank"' : '' ?>><?php echo $btnSites['title']; ?></a>
                    <h2><?php the_field('page_cours_sites_titre'); ?></h2>
                </div>
                <div class="sites">
                    <?php
                    $sites = get_field('page_cours_sites');
                    ?>

                    <?php foreach ($sites as $site) {
                        ?>
                        <?php $btnSite = $site['lien']; ?>


                        <div class="site">
                            <a href="<?php echo $btnSite['url']; ?>" <?php echo $btnSite['target'] != "" ? 'target="_blank"' : '' ?>
                               style="background-image: url('<?php echo $site['image']; ?>')">
                                <?php echo $site['texte'] ?>
                            </a>
                        </div>
                        <?php
                    } ?>
                </div>
            </div>
        </div>
    </section>

    <section id="cours-reservations">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2><?php the_field('page_cours_reservations_titre'); ?></h2>
                </div>
                <div class="reservations">
                    <?php
                    $reservations = get_field('page_cours_reservations');
                    ?>

                    <?php foreach ($reservations as $reservation) {
                        ?>
                        <?php $btnReservation = $reservation['lien_reservation']; ?>
                        <div class="reservation">
                            <div class="col1">
                                <?php echo $reservation['col1']; ?>
                            </div>
                            <div class="col2">
                                <?php echo $reservation['col2']; ?>
                            </div>
                            <div class="col3">
                                <a class="btn theme grand"
                                   href="<?php echo $btnReservation['url']; ?>" <?php echo $btnReservation['target'] != "" ? 'target="_blank"' : '' ?>>
                                    <?php echo $btnReservation['title'] ?>
                                </a>
                            </div>
                        </div>
                        <?php
                    } ?>
                </div>
            </div>
        </div>
    </section>

    <section id="cours-grise">
        <div class="container">
            <div class="grise">
                <div class="left">
                    <?php the_field('page_cours_grey_left'); ?>
                </div>
                <div class="right">
                    <?php the_field('page_cours_grey_right'); ?>
                </div>
            </div>
        </div>
    </section>

<?php get_sidebar("infolettre"); ?>

<?php get_footer(); ?>