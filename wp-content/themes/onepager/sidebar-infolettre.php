<section class="infolettre">

    <div class="container" role="main">

        <div class="row">


            <div class="newsletter">
                <div class="col-12 col-lg-5">
                    <h4><?php the_field('infolettre_titre', 'option'); ?></h4>
                        <p class="subtitle"><?php the_field('infolettre_text', 'option'); ?></p>
                </div>
                <div class="col-12 col-lg-7 position-initial d-flex align-items-center justify-content-center">
                <form method="post" target="_blank" action="https://ecolejmg.us10.list-manage.com/subscribe/post">
                    <input type="hidden" name="u" value="753e6e0d0f3195e68050b1aa8">
                    <input type="hidden" name="id" value="635e8ccece">
                    <input type="text" name="MERGE2" id="MERGE2" placeholder="Ton nom">
                    <input type="text" name="MERGE1" id="MERGE1" placeholder="Ton prénom">
                    <input type="email" autocapitalize="off" autocorrect="off" name="MERGE0" id="MERGE0"
                           placeholder="Ton adresse e-mail">
                    <div class="btn-submit"><button class="btn submit" type="submit" name="submit">Je m'inscris!</button></div>
                </form>
                </div>
            </div>

        </div>

    </div>

</section>