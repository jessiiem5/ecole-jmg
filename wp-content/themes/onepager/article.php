


<div>


    <?php

    $image = get_field('photo');
    $postid = get_the_ID();
    $activite = get_field('type', $postid);
    $public = get_field('public_cible', $postid);

    ?>

    <div class="img_cours" style="background-image: url('<?php echo $image['url'] ?>')"></div>

    <div class="row info-camps no-margin">

        <p class="titre-camps"><?php the_title(); ?></p>

        <?php if ($activite=="camps"){ ?>


            <!-- section pour l'affichage des camps -->

            <div class="col-sm-6">

                <p class="titre-infos-camp">Descriptif</p>
                <p><?php the_field('desc_min'); ?></p>

                <p class="titre-infos-camp">Activités</p>
                <p><?php the_field('activites'); ?></p>

                <p class="titre-infos-camp">Pourquoi nous?</p>
                <p><?php the_field('pourquoi'); ?></p>

                <p class="titre-infos-camp">Équipements</p>
                <p><?php the_field('equipements'); ?></p>

                <p class="titre-infos-camp">Journée type</p>
                <p><?php the_field('journee_type'); ?></p>



            </div>
            <div class="col-sm-6 col-droite-camps">

                <p class="titre-infos-camp">Particularités hebdomadaires</p>
                <p><?php the_field('particularites_hebdomadaires'); ?></p>

                <p class="titre-infos-camp">Horaire</p>
                <p><?php the_field('horaire'); ?></p>

                <p class="titre-infos-camp">Tarifs</p>
                <p><?php the_field('tarifs'); ?></p>


                <p class="titre-infos-camp">Notes</p>
                <p><?php the_field('notes'); ?></p>



                <p class="rdv-tel">inscription par téléphone <br><span>418-687-1250</span></p>

                <div id="popup_name" class="popup_block">
                    <h2>Merci de remplir le formulaire d'informations pour le Camp d'été.</h2>
                    <?php echo do_shortcode('[contact-form-7 id="495" title="Formulaire informations pour Camp"]'); ?>

                </div>


            </div>

        <?php } else { ?>


            <!-- section pour l'affichage des cours -->



            <div class="col-sm-6">

                <p class="titre-infos-camp">Descriptif</p>
                <p><?php the_field('desc_min'); ?></p>

                <p class="titre-infos-camp">Objectifs du cours</p>
                <p><?php the_field('obj'); ?></p>

                <p class="titre-infos-camp">Pourquoi nous?</p>
                <p><?php the_field('pourquoi'); ?></p>

                <p class="titre-infos-camp">Horaire</p>
                <p><?php the_field('horaire'); ?></p>


            </div>
            <div class="col-sm-6 col-droite-camps">

                <p class="titre-infos-camp">Tarifs</p>
                <p><?php the_field('tarifs'); ?></p>


                <p class="titre-infos-camp">Notes</p>
                <p><?php the_field('notes'); ?></p>



                <p class="rdv-tel">inscription par téléphone <br><span>418-687-1250</span></p>

                <div id="popup_name_cours" class="popup_block">
                    <h2>Merci de remplir le formulaire d'informations pour le cours junior.</h2>
                    <?php echo do_shortcode('[contact-form-7 id="583" title="Formulaire informations pour cours junior"]'); ?>

                </div>



            </div>

        <?php }  ?>

    </div>