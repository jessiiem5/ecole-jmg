<?php
/*
Template Name: Page générique
*/


get_header('section'); ?>

    <section id="generic-intro">
        <div class="phrase-background">
            <?php the_field('phrase_background'); ?>
        </div>
        <div class="container">
            <div class="row">
                <div class="text-center col-text">
                    <?php the_field('texte'); ?>
                </div>
            </div>
        </div>
    </section>

<?php $sections = get_field('sections');
if($sections)
{
    foreach ($sections as $index => $section) {
    ?>
    <section id="generic-section">
        <div class="col">
            <div class="col-lg-6 col-12 texte">
                <?php echo $section['text'] ?>
                </div>
            <div class="col-lg-6 col-12 image">
                <img src="<?php echo $section['image'] ?>"/>
            </div>
        </div>
    </section>
    <?php }} ?>


<?php get_sidebar( "infolettre" ); ?>

<?php get_footer(); ?>