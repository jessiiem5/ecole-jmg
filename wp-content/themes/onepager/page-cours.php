<?php
/*
Template Name: cours
*/


get_header('section');

// Selection de l'activité en fonction du slug de la page

global $post;
$activite = get_post( $post )->post_name;
($activite =="cours-de-tennis") ? $activite="tennis" : $activite="camps";

?>

    <section class="zone1">

        <div class="container-fluid fond_blanc" role="main">

            <div class="row no-margin">


                <div class="row no-margin">

                    <div class="col-xs-12 col-sm-4">

                        <div class="row">

                            <?php if ($activite=="camps") {


                            // Selection des cours pour adulte
                            $args = array(
                                'post_type' => 'cours',
                                'meta_key' => 'type',
                                'meta_value' => 'camps'

                            );

                            $the_query = new WP_Query( $args );

                            while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


                                <div class="col-sm-12 rdv-cours">

                                    <p class="titre-cours"><?php the_title(); ?></p>


                                    <div class="row info-cours hidden-xs">

                                        <div class="col-sm-8">

                                            <p>Sommaire</p>
                                            <p><?php the_field('sommaire'); ?></p>

                                        </div>
                                        <div class="col-sm-4">

                                            <p>Niveau</p>
                                            <p><?php the_field('niveau'); ?></p>

                                        </div>

                                    </div>
                                    <a class="btn theme moyen btn-cours" href="#" data-id="<?php echo get_the_ID();?>" data-type="<?php echo $activite;?>" >En savoir plus</a>

                                    <p class="rdv-tel hidden-xs">inscription par téléphone <br><span>418-687-1250</span></p>

                                </div>

                            <?php   endwhile;

                                    wp_reset_query();

                            } else {


                             ?>


                            <div role="tabpanel" class="onglet">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#adultes" aria-controls="adultes" role="tab" data-toggle="tab">Adultes</a></li>
                                    <li role="presentation"><a href="#juniors" aria-controls="juniors" role="tab" data-toggle="tab">Juniors</a></li>
                                    <li role="presentation"><a href="#prive" aria-controls="prive" role="tab" data-toggle="tab">Privés</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">

                                    <div role="tabpanel" class="tab-pane active" id="adultes">

                                        <?php

                                        // Selection des cours pour adulte
                                        $args = array(
                                            'numberposts' => -1,
                                            'post_type' => 'cours',
                                            'meta_query' => array(
                                                'relation' => 'AND',
                                                array(
                                                    'key' => 'type',
                                                    'value' => $activite,
                                                    'compare' => '='
                                                ),
                                                array(
                                                    'key' => 'public_cible',
                                                    'value' => 'senior',
                                                    'compare' => '='
                                                ))
                                        );

                                        $the_query = new WP_Query( $args );

                                         while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


                                                <div class="col-sm-12 rdv-cours">

                                                    <p class="titre-cours"><?php the_title(); ?></p>


                                                    <div class="row info-cours hidden-xs">

                                                        <div class="col-sm-8">

                                                            <p>Sommaire</p>
                                                            <p><?php the_field('sommaire'); ?></p>

                                                        </div>
                                                        <div class="col-sm-4">

                                                            <p>Niveau</p>
                                                            <p><?php the_field('niveau'); ?></p>

                                                        </div>

                                                    </div>
						    <a class="btn theme moyen" target="blank" href="http://ecolejmg.com/wp-content/uploads/2016/01/autoevaluation.pdf" >Définir mon niveau</a>
                                                    <a class="btn theme moyen btn-cours" href="#" data-id="<?php echo get_the_ID();?>" data-type="<?php echo $activite;?>">En savoir plus</a>

                                                    <p class="rdv-tel hidden-xs">inscription par téléphone <br><span>418-687-1250</span></p>

                                                </div>

                                         <?php endwhile; ?>



                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="juniors">

                                        <?php

                                        // Selection des cours pour enfants.
                                        $args = array(
                                            'numberposts' => -1,
                                            'post_type' => 'cours',
                                            'meta_query' => array(
                                                'relation' => 'AND',
                                                array(
                                                    'key' => 'type',
                                                    'value' => $activite,
                                                    'compare' => '='
                                                ),
                                                array(
                                                    'key' => 'public_cible',
                                                    'value' => 'junior',
                                                    'compare' => '='
                                                ))
                                        );

                                        $the_query = new WP_Query( $args );

                                        while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


                                            <div class="col-sm-12 rdv-cours">

                                                <p class="titre-cours"><?php the_title(); ?></p>


                                                <div class="row info-cours hidden-xs">

                                                    <div class="col-sm-8">

                                                        <p>Sommaire</p>
                                                        <p><?php the_field('sommaire'); ?></p>

                                                    </div>
                                                    <div class="col-sm-4">

                                                        <p>Niveau</p>
                                                        <p><?php the_field('niveau'); ?></p>

                                                    </div>

                                                </div>

                                                <a class="btn theme moyen btn-cours" href="#" data-id="<?php echo get_the_ID();?>" data-type="<?php echo $activite;?>" >En savoir plus</a>

                                                <p class="rdv-tel hidden-xs">inscription par téléphone <br><span>418-687-1250</span></p>

                                            </div>

                                        <?php endwhile; ?>




                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="prive">

                                        <?php

                                        // Selection des cours privés.
                                        $args = array(
                                            'numberposts' => -1,
                                            'post_type' => 'cours',
                                            'meta_query' => array(
                                                'relation' => 'AND',
                                                array(
                                                    'key' => 'type',
                                                    'value' => $activite,
                                                    'compare' => '='
                                                ),
                                                array(
                                                    'key' => 'public_cible',
                                                    'value' => 'prive',
                                                    'compare' => '='
                                                ))
                                        );

                                        $the_query = new WP_Query( $args );

                                        while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


                                            <div class="col-sm-12 rdv-cours">

                                                <p class="titre-cours"><?php the_title(); ?></p>


                                                <div class="row info-cours hidden-xs">

                                                    <div class="col-sm-8">

                                                        <p>Sommaire</p>
                                                        <p><?php the_field('sommaire'); ?></p>

                                                    </div>
                                                    <div class="col-sm-4">

                                                        <p>Niveau</p>
                                                        <p><?php the_field('niveau'); ?></p>

                                                    </div>

                                                </div>

                                                <a class="btn theme moyen btn-cours" href="#" data-id="<?php echo get_the_ID();?>" >En savoir plus</a>

                                                <p class="rdv-tel hidden-xs">inscription par téléphone <br><span>418-687-1250</span></p>

                                            </div>

                                        <?php endwhile; ?>




                                    </div>

                                </div>

                            </div>

                        <?php  } ?>

                        </div>

                    </div>


                    <div class="col-xs-12  col-sm-8   camp-dete no-padding">

                        <?php


                            $args = array(
                                'posts_per_page' => 1,
                                'post_type' => 'cours',
                                'meta_key' => 'type',
                                'meta_value' => $activite,
                                'orderby' => 'rand'
                            );


                        $the_query = new WP_Query( $args );

                        while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                        <?php get_template_part('article'); ?>


                        </div>

                    </div>

                    <?php endwhile; ?>

                </div>

            </div>

    </section>

<?php get_sidebar( "infolettre" ); ?>

<?php get_footer(); ?>