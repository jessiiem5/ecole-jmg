<?php
/*
Template Name: blog
*/


get_header('section'); ?>


<?php

global $post;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1 ;
$article = get_post( $post )->post_name;

    if ($article=="evenements") {

    // Selection des articles d'evenements
    $args = array(
        'paged' => $paged,
        'type' => 'post',
        'cat' => 5,
        'posts_per_page' => -1
    );

    } else if ($article=="cohue") {

    // Selection des articles a propos
    $args = array(
        'paged' => $paged,
        'type' => 'post',
        'cat' => 10,
        'posts_per_page' => -1
    );

    } else if ($article=="a-propos") {

        // Selection des articles a propos
        $args = array(
            'paged' => $paged,
            'type' => 'post',
            'cat' => 9,
            'posts_per_page' => -1
        );

    } else if ($article=="nospros") {

        // Selection des articles des pros
        $args = array(
            'paged' => $paged,
            'type' => 'post',
            'cat' => 8,
            'posts_per_page' => -1
        );

    } else {

    $args = array(
        'paged' => $paged,
        'type' => 'post',
        'category__not_in' => array(5,8,9),
        'posts_per_page' => -1
    );

    }

    $the_query = new WP_Query( $args );

   ?>


    <section class="zone1 clearfix">


    <?php if($article=="nospros"): ?>
				<div class="container">
          <p class="titre-camps" style="padding-top:100px;">Nos pros</p>
          <div class="row">
					<?php

                    $i=1;
                    while ( $the_query->have_posts() ) : $the_query->the_post();

                        $image = get_field('image_article');
                        $categories = get_the_category($post->ID);
                        $image_raquette = get_field('image_raquette');
                        ?>

              <div class="equipe col-12 col-md-4">
                    <div style='padding: 10px 10px;'>
										<div class="col-12 article1 <?php if  ($i%2 == 0) echo ('col-sm-push-6') ?> "  style="background-image: linear-gradient(rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.5)) , url('<?php echo $image['url']?>'); " >
										</div>
                    <div class="textearticle">
                      <h3 class="titre white"><?php the_title() ?></h3>
                      <h4 class="titre white"><?php the_field('fonction') ?></h4>
                      <hr class="photo-hr">
                      <div class="raquette-pros" style="padding:10px;">
                        <a href="<?php the_field('lien')?>" target="_blank">
                        	<h4><?php the_field('first_title') ?></h4>
                          <img src="<?= $image_raquette['url']?>" alt="<?= $image_raquette['title']?>">
                        </a>
                          <hr>
                          <?php the_content(); ?>
                      </div>
                    </div>
										</div>
            </div>
        <?php $i++;
        endwhile;
        ?>
				</div>
      </div>


  <?php else: ?>

		 <div class="container-fluid fond_blanc" role="main">
                    <?php

                    $i=1;
                    while ( $the_query->have_posts() ) : $the_query->the_post();

                        $image = get_field('image_article');
                        $categories = get_the_category($post->ID);

                        ?>
                        <div>

                            <div class="col-sm-6  article1 <?php if  ($i%2 == 0) echo ('col-sm-push-6') ?> "  style="background-image: url('<?php echo $image['url']?>'); " ></div>

                            <div class="col-sm-6  article-texte <?php if  ($i%2 == 0) echo ('col-sm-pull-6') ?>">

                                <div>

                                    <?php if (!is_page(array(132,176,27,502))) { ?>

                                        <div class="clearfix"><span><?php the_time('j F Y') ?></span><span><?php echo $categories[0]->cat_name; ?></span></div>

                                    <?php } ?>

                                    <div class="article-cont" >

                                        <div class="article-infos" >

                                            <p class="titre"><?php the_title() ?></p>
                                            <p class="fonction"><?php the_field('fonction') ?></p>
                                            <p class="texte"> <?php if (is_page(array(132,176,502))) { the_content();} else  { the_excerpt();} ?></p>

                                        </div>

                                        <?php if (!is_page(array(132,176,502))) { echo '<a class="btn theme moyen" href="'.get_permalink().'">Lire la suite</a>';} ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $i++;
                    endwhile;
                   ?>

                <?php endif; ?>
            </div>
    </section>

<?php get_sidebar( "infolettre" ); ?>

<?php get_footer(); ?>
