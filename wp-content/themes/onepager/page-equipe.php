<?php
/*
Template Name: Equipe
*/


get_header(); ?>


<section id="slider-equipe" style="background-image:url('<?php echo get_field('image_de_fond')['url']; ?>')">
    <div class="inner-slider-cours">
        <h1><?php the_field('titre'); ?></h1>
        <p class="subtitle"><?php the_field('sous-titre'); ?></p>
    </div>
</section>
<section id="equipe">
    <div class="container">
        <div class="grid">
            <?php $team = get_posts(array(
                'numberposts' => -1,
                'post_type' => 'equipe')); ?>

            <?php foreach ($team as $teamMember) {
                ?>
                <div class="member" data-id="<?php echo $teamMember->ID; ?>">
                    <div class="inner"
                         style="background-image: url('<?php echo get_the_post_thumbnail_url($teamMember->ID, 'full'); ?>')">
                        <?php if ($teamMember->lien_cours) { ?>

                            <a class="lien-cours" href="<?php echo $teamMember->lien_cours['url']; ?>"></a>
                            <?php
                        } ?>
                        <h3 class="name"><?php echo $teamMember->post_title ?></h3>
                        <div class="hover">
                            <h3 class="name"><?php echo $teamMember->post_title ?></h3>
                            <p><?php echo $teamMember->team_phrase ?></p>
                            <div class="btn-team"><span>Voir le profil</span></div>
                        </div>
                    </div>
                </div>
                <?php
            } ?>
        </div>
    </div>
</section>


<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">Fermer<img
                    src="<?php echo get_home_url(); ?>/wp-content/themes/onepager/assets/img/icon.x.svg"/></span>
        <div class="inner-modal">

            <div class="photo">
                <h2></h2>
                <a class="lien-membre-precedent desktop-show mobile-hide member" data-id=""></a>
            </div>
            <div class="text">
                <h2></h2>
                <h3></h3>

                <a class="lien-membre-precedent desktop-hide mobile-show member" data-id=""></a>
                <a class="lien-membre-suivant member" data-id=""></a></div>

        </div>
    </div>

</div>

<?php get_sidebar("infolettre"); ?>

<?php get_footer(); ?>
