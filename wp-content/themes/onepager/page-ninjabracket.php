<?php
/*
Template Name: Ninja Bracket
*/

get_header('section');

?>

<section class="" style="background-color: #fff">
	<?php the_field('code_ninja_bracket'); ?>
</section>

<script type="text/javascript">
(function() {
  var script = document.createElement('script'); 
	script.type = 'text/javascript';
	script.src = 'https://cdn.commoninja.com/sdk/latest/commonninja.js';
	document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>

<?php get_footer();  ?>