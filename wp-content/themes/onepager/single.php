<?php



get_header('shop'); ?>

    <section id="slider-default" style="background-image:url('<?php echo get_field('bg_header','option'); ?>')">
        <div class="inner-slider-cours">
            <h1 class="h2"><?php the_title();?></h1>
        </div>
    </section>

    <section id="page">
        <div class="container">
            <div class="row">

                <?php while (have_posts()) : the_post(); ?>



                    <?php the_content(); ?>

                <?php endwhile; // end of the loop. ?>

            </div><!-- #main -->
        </div><!-- #primary -->
    </section>


<?php get_sidebar( "infolettre" ); ?>

<?php get_footer(); ?>