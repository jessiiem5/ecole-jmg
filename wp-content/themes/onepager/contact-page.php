<?php
/*
Template Name: contact
*/


get_header('section'); ?>


<section class="zone1">

    <div class="container-fluid fond_blanc" role="main">



                <div class="col-sm-4 section-contact">

                    <p class="titre">École JMG<br/> Tennis Montcalm</p>
                    <p class="entete">Service à la clientèle</p>
                    <p>418-687-1250</p>
                    <p class="entete">Par courriel</p>
                    <p>info@ecolejmg.com</p>
                    <p class="entete">Par la poste</p>
                    <p>1141, boulevard Champlain</p>
                    <p>Québec QC G1K 0A2</p>

                    <a href="https://www.google.ca/maps/@46.7878609,-71.2332789,263m/data=!3m1!1e3" target="_blank">Voir la carte</a>

                </div>


                <div class="col-sm-4 section-contact no-padding">

                    <p class="titre">Écrivez-nous</p>

                    <?php echo do_shortcode( '[contact-form-7 id="21" title="Formulaire de contact"]' ) ?>

                </div>

                <div class="col-sm-4 section-contact">

                    <p class="titre">Horaire  d'été</p>

                    <p>1er juin au 31 août</p>

                    <div class="row">

                        <div class="col-sm-6">
                            <p class="entete">Lundi au vendredi</p>
                            <p>7h à 22h30</p>
                        </div>

                        <div class="col-sm-6">
                            <p class="entete">Samedi et dimanche</p>
                            <p>8h à 18h45</p>
                        </div>

                    </div>

                    <p class="titre">Horaire d'hiver</p>
                    <p>1er septembre au 31 mai</p>

                    <div class="row">

                        <div class="col-sm-6">
                            <p class="entete">Lundi au vendredi</p>
                            <p>7h à 23h00*</p>
                        </div>

                        <div class="col-sm-6">
                            <p class="entete">Samedi et dimanche</p>
                            <p>8h à 23h00*</p>
                        </div>

                    </div>

                    <p>1er septembre au 31 mai</p>
                    <p class="location">*dernière heure de location: 22h00 à 23h00</p>

                </div>

            </div>



</section>

<?php get_sidebar( "infolettre" ); ?>

<?php get_footer(); ?>