<?php
/*
Template Name: galerie
*/


get_header('section'); ?>


<section id="galerie">
  <div class="container">
    <p class="titre-camps" style="padding-top:100px;">Galerie Photo</p>
    <div class="actus mb-5" data-js="hero-demo">
          <div class="ui-group">
            <div class="filters button-group js-radio-button-group">
                <button class="button is-checked mb-4" data-filter="*">Tous</button>
                    <!--stockage des catégories des articles-->
                    <?php $cats = get_terms(array('taxonomy' => 'custom_taxonomy_galerie')); ?>
                    <!--Affichage du nom et slug de chaque catégorie-->
                    <?php foreach( $cats as $cat) : ?>
                      <button class="button mb-4 mr-2" data-filter="<?php echo '.'.$cat->slug;  ?>"><?php echo $cat->name;?></button>
                    <?php  endforeach;?>
                    <!--Fin Affichage du nom et slug de chaque catégorie-->
            </div>
          </div>
          <?php wp_reset_query();?>
          <?php $posts = get_posts( array(
      	      'post_type'         => 'galerie',
      	      'posts_per_'    => 50,
      	      'orderby'           => 'date',
      	      'order'             => 'asc',
      	      'suppress_filters' => 0 ));
      	  ?>
        <?php $cpt=0 ?>
        <?php if( $posts ): ?>
            <div class="anim row grid" style="margin: 0px;">
                  <?php foreach( $posts as $post ): ?>
                      <?php setup_postdata( $post ); ?>
                      <?php
                      $categories = get_the_terms($post->ID,array('taxonomy' => 'custom_taxonomy_galerie'));
                      $images = get_field('image_galerie');
                	?>
        						<div class="col-12 col-md-6 col-lg-4 element-item no-gutters <?php foreach( $categories as $category ) : echo $category->slug . ' '; endforeach; ?>" data-category="<?php foreach( $categories as $category ) : echo $category->slug . ' '; endforeach; ?>" style="margin-top:3px; padding:0px 2px;">
                      <a href="<?php the_permalink(); ?>">
        							     <img src="<?= $images['url'] ?>" alt="<?=  $images['title'] ?>">
                      </a>
        						</div>
        				<?php endforeach; wp_reset_postdata(); ?>
              </div>
    			<?php endif; ?>
    </div>
    </div>
</section>

<?php get_sidebar( "infolettre" ); ?>

<?php get_footer(); ?>
