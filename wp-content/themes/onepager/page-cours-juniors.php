<?php
/*
Template Name: juniors
*/


get_header('section');

// Selection de l'activité en fonction du slug de la page

global $post;
$url = get_post($post)->post_name;
$post_ID = get_post($post)->ID;
($url == "adultes") ? $public = "senior" : "";
($url == "juniors-new") ? $public = "junior" : "";
($url == "prives") ? $public = "prive" : "";

$ID = wp_get_post_parent_id($post_ID);
($ID == 15) ? $activite = "tennis" : $activite = "camps";

?>

    <section id="cours-intro">
        <div class="phrase-background">
            <?php the_field('phrase_background'); ?>
        </div>
        <div class="container">
            <div class="row">
                <div class="text-center col-text">
                    <?php the_field('texte_1'); ?>
                </div>
            </div>
        </div>
    </section>

    <section id="cours-infos" class="cours-junior">
        <div class="col">
            <div class="col-lg-6 col-12 texte">
                <h2><?php the_field('titre_saison'); ?></h2>
                <div class="grid">
                    <div class="left">
                        <h3>Durée:</h3>
                    </div>
                    <div class="right">
                        <p><?php the_field('durée_saison'); ?></p>
                    </div>
                    <div class="left">
                        <h3>Dates:</h3>
                    </div>
                    <div class="right">
                        <p><?php the_field('date_saison'); ?></p>
                    </div>
                    <div class="left">
                        <h3><?php the_field('titre_horaire'); ?>:</h3>
                    </div>
                    <div class="right"><p>
                            <?php

                            // check if the repeater field has rows of data
                            if (have_rows('horaires')):

                                // loop through the rows of data
                                while (have_rows('horaires')) : the_row();

                                    // display a sub field value
                                    ?>
                                    <?php the_sub_field('nom'); ?>: <?php the_sub_field('horaire'); ?></br>
                                <?php

                                endwhile;


                            endif;

                            ?>
                        </p>
                    </div>
                </div>
                <?php $inscription = get_field('inscription');
                if ($inscription) {
                    ?>
                    <?php $btnInfos = get_field('junior_bouton'); ?>
                    <a class="btn theme grand"
                       href="<?php echo $btnInfos['url']; ?>" <?php echo $btnInfos['target'] != "" ? 'target="_blank"' : '' ?>><?php echo $btnInfos['title']; ?></a>
                    <?php
                }
                ?>
            </div>
            <div class="col-lg-6 col-12 image">
                <img src="<?php the_field('image_1'); ?>"/>
            </div>
        </div>
    </section>

    <section id="cours-juniors-reservations">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2><?php the_field('titre_cours'); ?></h2>
                </div>
                <div class="cours-juniors">
                    <?php
                    $args = array(
                        'post_type' => 'cours',
                        'meta_query' => array(
                            array(
                                'key' => 'type',
                                'value' => $activite,
                            ),
                            array(
                                'key' => 'public_cible',
                                'value' => $public,
                            ),
                        ),
                    );

                    $the_query = new WP_Query($args);

                    while ($the_query->have_posts()) :
                        $the_query->the_post();

                        $postid = get_the_ID();
                        $activite = get_field('type', $postid);
                        $public = get_field('public_cible', $postid);
                        $btnReserve = get_field('reserve_btn', $postid);

                        ?>

                        <div class="accordion">
                            <div class="accordion-header">
                                <div class="title">
                                    <?php the_title(); ?><img src="<?php echo get_home_url(); ?>/wp-content/themes/onepager/assets/img/icon.chevrondown.svg"/>
                                    <div class="horaire-mobile">
                                        <img src="<?php echo get_home_url(); ?>/wp-content/themes/onepager/assets/img/icon.journee.black.svg"/>
                                        <p><?php the_field('horaire'); ?></p>
                                    </div>
                                    <div class="age-mobile">
                                        <img src="<?php echo get_home_url(); ?>/wp-content/themes/onepager/assets/img/icon.responsables.black.svg"/>
                                        <p><?php the_field('groupe_age'); ?></p>
                                    </div>
                                </div>
                                <div class="horaire">
                                    <img src="<?php echo get_home_url(); ?>/wp-content/themes/onepager/assets/img/icon.journee.black.svg"/>
                                    <p><?php the_field('horaire'); ?></p>
                                </div>
                                <div class="age">
                                    <img src="<?php echo get_home_url(); ?>/wp-content/themes/onepager/assets/img/icon.responsables.black.svg"/>
                                    <p><?php the_field('groupe_age'); ?></p>
                                </div>
                                <div class="reserver">
                                    <?php $btnInfos = get_field('reserve_btn'); ?>
                                    <a class="btn theme grand"
                                       href="<?php echo $btnInfos['url']; ?>" <?php echo $btnInfos['target'] != "" ? 'target="_blank"' : '' ?>>Réserver
                                        ma place</a>
                                </div>
                            </div>
                            <div class="panel" style="background-image:url(<?php the_field('raquette'); ?>);">
                                <div class="col1">
                                    <p class="titre-infos-camp">Objectifs du cours</p>
                                    <p><?php the_field('obj'); ?></p>
                                </div>
                                <div class="col2">
                                    <p class="titre-infos-camp">Terrain</p>
                                    <p><?php the_field('terrain'); ?></p>
                                    <img src="<?php the_field('image_terrain'); ?>">
                                </div>
                                <div class="col3">
                                    <p class="titre-infos-camp">Balles</p>
                                    <img style="max-width:110px;margin-bottom:25px;" src="<?php the_field('image_balles'); ?>">

                                    <p class="titre-infos-camp">Places</p>
                                    <p><?php the_field('places'); ?></p>
                                </div>
                                <div class="col4">
                                    <p class="titre-infos-camp">Prix</p>
                                    <p class="price"><?php the_field('tarifs'); ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>

<?php get_sidebar("infolettre"); ?>

<?php get_footer(); ?>