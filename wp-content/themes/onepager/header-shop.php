<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php bloginfo('name') ?> | <?php the_title() ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet">

    <link rel="apple-touch-icon" sizes="57x57"
          href="<?php bloginfo('template_url'); ?>/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60"
          href="<?php bloginfo('template_url'); ?>/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72"
          href="<?php bloginfo('template_url'); ?>/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76"
          href="<?php bloginfo('template_url'); ?>/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="<?php bloginfo('template_url'); ?>/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120"
          href="<?php bloginfo('template_url'); ?>/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144"
          href="<?php bloginfo('template_url'); ?>/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152"
          href="<?php bloginfo('template_url'); ?>/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180"
          href="<?php bloginfo('template_url'); ?>/favicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/favicons/favicon-32x32.png"
          sizes="32x32">
    <link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/favicons/favicon-194x194.png"
          sizes="194x194">
    <link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/favicons/favicon-96x96.png"
          sizes="96x96">
    <link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/favicons/android-chrome-192x192.png"
          sizes="192x192">
    <link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/favicons/favicon-16x16.png"
          sizes="16x16">
    <link rel="manifest" href="<?php bloginfo('template_url'); ?>/favicons/manifest.json">
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicons/favicon.ico">
    <link rel="stylesheet" href="https://use.typekit.net/qqk5lib.css">

    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-TileImage" content="<?php bloginfo('template_url'); ?>/favicons/mstile-144x144.png">
    <meta name="msapplication-config" content="<?php bloginfo('template_url'); ?>/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>


    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<?php

$image = get_field('bg_header','option');

?>

<header class="main-menu">

    <!--video autoplay loop id="myVideo" muted>
        <source src="http://ecolejmg.com/wp-content/themes/onepager/images/video.webm" type="video/webm">
        <source src="http://ecolejmg.com/wp-content/themes/onepager/images/video.mp4" type="video/mp4">
    </video-->


    <div class="row d-flex align-items-center">

        <div class="col-sm-3 logo">
            <a href="<?php echo get_home_url(); ?>">
                <img src="<?php the_field('site_logo', 'option'); ?>"/>
            </a>
        </div>

        <div class="col-sm-9 menu-nav">
            <div class="d-flex justify-content-end">
                <nav class="navbar navbar-default  headder-top" role="navigation">


                        <?php $walker = new Menu_With_Description; ?>
                        <?php echo wp_nav_menu(array('theme_location' => 'top', 'container' => false, 'menu_class' => 'nav navbar-nav navbar-left')); ?>

                </nav>

                <div class="shopping-bag">
                    <a href="<?php echo wc_get_cart_url(); ?>">
                        <img src="<?php echo get_home_url(); ?>/wp-content/themes/onepager/assets/img/icon.bag.svg">
                    </a>
                </div>

                <div class="nav-button-holder mobile ">
                    <div class="nav-button vis-m">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <!--div class="slide container-fluid">

		<p class="titre">Inscription au cours de tennis</p>
		<p class="accroche">Des groupes pour tous les niveaux</p>
		<p class="texte">Venez perfectioner votre tennis avec la meilleure équipe de pros de la région de québec.</p>
		<a class="btn transparent" href="<?php bloginfo('url') ?>/cours-de-tennis/">Inscription !</a>
		a class="btn transparent inverse" href="<?php bloginfo('url') ?>/cours-de-tennis">Cours de tennis</a>

	</div>-->


    <!-- Ninja Bracket -->
    <script type="text/javascript">
        (function () {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = 'https://cdn.commoninja.com/sdk/latest/commonninja.js';
            document.getElementsByTagName('head')[0].appendChild(script);
        })();
    </script>


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-62684536-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-62684536-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '535297336845285');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=535297336845285&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->

</header>
