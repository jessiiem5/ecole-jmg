<?php
/*
Template Name: Defis
*/


get_header('section');

// Selection de l'activité en fonction du slug de la page



?>

    <section class="zone1">

        <div class="container-fluid fond_blanc" role="main">

            <div class="row no-margin">


                <div class="row no-margin">

                    <div class="col-xs-12 col-sm-4">

                        <div class="row">
                            <?php if( have_rows('publicite') ): ?>



                                    <?php while( have_rows('publicite') ): the_row();

                                        // vars
                                        $image = get_sub_field('image');
                                        $link = get_sub_field('lien');

                                        ?>

                                            <?php if( $link ): ?>
                                            <a href="<?php echo $link; ?>">
                                                <?php endif; ?>

                                                <img src="<?php echo $image['url']; ?>" width="100%" alt="<?php echo $image['alt'] ?>" />

                                                <?php if( $link ): ?>
                                            </a>
                                        <?php endif; ?>





                                    <?php endwhile; ?>


                            <?php endif; ?>

                        </div>

                    </div>

                    <div class="col-xs-12  col-sm-8   camp-dete no-padding">

                        <div>

                        <?php if (!post_password_required()) {

                        ?>

                            <?php

                            $image = get_field('image_haut');

                            if( !empty($image) ): ?>

                            <div class="img_cours" style="background-image: url('<?php echo $image['url'] ?>')"></div>

                            <?php endif; ?>

                            <div class="row info-camps no-margin">

                                <div class="clearfix">
                                    <div class="ligue_info">
                                        <p class="titre-camps"><?php the_field("titre_article"); ?></p>
                                        <p><?php the_field("commentaire"); ?></p>
                                    </div>
                                    <?php if( is_page(1524)) { ?>
                                    <a class="btn theme fix match" href="<?php bloginfo('url')?>/match-tennis-ligue-echelle/">PLANIFIER<br>UN MATCH</a>
                                    <?php } ?>
                                </div>

                                <?php if( is_page(1524)) { ?>

                                <p class="class_title">Classement des joueurs</p>

                                <?php  }

                                $image = get_field('image_haut');

                                if( is_page(1526) ) { ?>

                                <div class="classement_jour">

                                    <?php if( have_rows('joueur') ): ?>
                                        <?php $number=1; while( have_rows('joueur') ): the_row();
                                            // vars
                                            $nom = get_sub_field('nom_joueur');
                                            $link = get_sub_field('lien'); ?>
                                            <?php if( $link ): ?>
                                                <a href="<?php echo $link; ?>">
                                            <?php endif; ?>

                                            <p><?php echo $number; ?>.  <?php echo $nom; ?></p>

                                            <?php if( $link ): ?>
                                                </a>
                                            <?php endif; ?>

                                        <?php
                                            $number++;

                                        endwhile; ?>

                                    <?php endif; ?>

                                </div>

                                <?php }


                                if( is_page(1526) ) { ?>

                                <div class="coord_joueur">


                                    <ul>
                                        <li class="class_title">Nom du joueur</li>
                                        <li class="class_title">Numéro de téléphone</li>
                                        <li class="class_title">Adresse courriel</li>
                                    </ul>
                                    <?php if( have_rows('joueur') ): ?>

                                        <?php $number=1; 
                                        
                                        while( have_rows('joueur') ): the_row();
                                            // vars
                                            $nom = get_sub_field('nom_joueur');
                                            $phone = get_sub_field('phone');
                                            $mail = get_sub_field('courriel');

                                            ?>
                                            <ul class="list_joueur">
                                                <li><?php echo $nom; ?></li>
                                                <li><?php echo $phone; ?></li>
                                                <li><?php echo $mail; ?></li>
                                            </ul>
                                            <?php
                                        endwhile; ?>

                                    <?php endif; ?>

                                </div>

                                <?php }  ?>

                            </div>
                    </div>

                 <?php   } else { ?>
                    <div class="mdp">
                        <?php  the_content(); }?>
                    </div>
                </div>

            </div>

    </section>

<?php get_sidebar( "infolettre" ); ?>

<?php get_footer(); ?>