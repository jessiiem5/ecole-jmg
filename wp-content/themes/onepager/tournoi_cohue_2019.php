<?php
/*
Template Name: tournoi cohue 2019
*/

get_header('section');

?>
<section id="inscription-tournoi-achat">
    <div class="container">
		<div class="col-12">
                <h1 class="titre-camps">Inscription au tournoi 2019</h1>
				<?php the_content() ?>
            </div>
        <div class="row">
            
            <?php if (have_rows('repeater_tournoi')) : $index = 1;  while (have_rows('repeater_tournoi')) : the_row(); ?>

                <div class="inscription-cours<?php the_sub_field('cours_ou_diner') ?> col-md-4">
                  <div id="inscription-overlay"></div>
				<?php $imgCours = get_sub_field('image_categorie'); ?>
                  <div class="inscription-inner" style='background-image:linear-gradient(rgba(0,0,0,0.3),rgba(0,0,0,0.3)), url(<?= $imgCours['url']; ?>);background-size: cover; background-repeat: no-repeat; background-position: top center;'>
                    <h3><?php the_sub_field('nom_categorie'); ?></h3>
                    <h4><?php the_sub_field('prix_de_la_categorie'); ?>$</h4>
                    
                    <img src="<?= $imgCours; ?>" alt="">

                    </div>
                </div>
            <?php $index++; endwhile; endif; ?>
        </div>
    </div>
</section>


<?php get_sidebar("infolettre"); ?>

<?php get_footer(); ?>
<script>
  fbq('track', 'Purchase');
</script>
