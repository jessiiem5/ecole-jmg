<?php
/*
Template Name: coursv2
*/


get_header('section');

// Selection de l'activité en fonction du slug de la page

global $post;
$url = get_post( $post )->post_name;
($url=="adultes")? $public="senior":"";
($url=="juniors")? $public="junior":"";
($url=="prives")? $public="prive":"";

$ID=wp_get_post_parent_id( $post->ID );
($ID==15)? $activite="tennis": $activite="camps";

?>

    <section class="zone1">
        <div class="row info-camps no-margin">

            <p class="titre-camps"><?php the_field('titre_info'); ?></p>


            <div class="col-sm-3">

                <p class="titre-infos-camp"><?php the_field('sous_titre_1'); ?></p>
                <p><?php the_field('texte_1'); ?></p>
            </div>
            <div class="col-sm-3">

                <p class="titre-infos-camp"><?php the_field('sous_titre_2'); ?></p>
                <p><?php the_field('texte_2'); ?></p>


            </div>
            <div class="col-sm-3 col-droite-camps">

                <p class="titre-infos-camp"><?php the_field('sous_titre_3'); ?></p>
                <p><?php the_field('texte_3'); ?></p>

            </div>

            <?php if ($activite=="tennis"){ ?>
            <div class="col-sm-3 bloc_info">

                <p><b>Définir votre niveau facilement</b></p>

                <p>Utilisez notre tableau pour évaluer votre niveau facilement et ainsi vous inscrire dans la catégorie qui correspond à vos aptitudes.</p>

                <a href="http://ecolejmg.com/wp-content/uploads/2016/01/autoevaluation.pdf" class="btn theme moyen" target="_blank">Définir mon niveau</a>

            </div>
             <?php } else { ?>

            <div class="col-sm-3">

                <p class="titre-infos-camp"><?php the_field('sous_titre_4'); ?></p>
                <p><?php the_field('texte_4'); ?></p>

            </div>

            <?php }  ?>

        </div>
        <div class="bande_rouge">
            <!--<p>En savoir plus</p>-->
        </div>

            <div class="row no-margin">


                <div class="row no-margin">



                    <div class="col-xs-12  col-sm-12   camp-dete no-padding">

                        <?php
                            $args = array(
                                'post_type' => 'cours',
                                'meta_query' => array(
                                    array(
                                        'key' => 'type',
                                        'value' => $activite,
                                    ),
                                    array(
                                        'key' => 'public_cible',
                                        'value' => $public,
                                    ),
                                ),
                            );

                        $the_query = new WP_Query( $args );

                        while ( $the_query->have_posts() ) : $the_query->the_post(); ?>





                            <?php

                            $image = get_field('photo');
                            $postid = get_the_ID();
                            $activite = get_field('type', $postid);
                            $public = get_field('public_cible', $postid);

                            ?>


                            <div class="row info-camps no-margin">

                                <p class="titre-camps"><?php the_title(); ?></p>

                                <?php if ($activite=="camps"){ ?>



                                    <!-- section pour l'affichage des camps -->

                                    <div class="col-sm-3">

                                        <p class="titre-infos-camp">Descriptif</p>
                                        <p><?php the_field('desc_min'); ?></p>

                                        <p class="titre-infos-camp">Activités</p>
                                        <p><?php the_field('activites'); ?></p>


                                        <!--p class="titre-infos-camp">Pourquoi nous?</p>
                                        <p><?php the_field('pourquoi'); ?></p>

                                        <p class="titre-infos-camp">Équipements</p>
                                        <p><?php the_field('equipements'); ?></p>

                                        <p class="titre-infos-camp">Journée type</p>
                                        <p><?php the_field('journee_type'); ?></p-->



                                    </div>
                                    <div class="col-sm-3 col-droite-camps">

                                        <p class="titre-infos-camp">Particularités hebdomadaires</p>
                                        <p><?php the_field('particularites_hebdomadaires'); ?></p>

                                        <!--p class="titre-infos-camp">Horaire</p>
                                        <p><?php the_field('horaire'); ?></p-->



                                    </div>
                                    <div class="col-sm-3 ">
                                        <p class="titre-infos-camp">Tarifs</p>
                                        <p><?php the_field('tarifs'); ?></p>


                                        <p class="titre-infos-camp">Notes</p>
                                        <p><?php the_field('notes'); ?></p>
                                    </div>
                                    <div class="col-sm-3 bloc_inscr">
                                        <!--selection des cours junior pour l'affichage du popup d'inscription -->

                                        <p class="slogan">Inscrivez-vous à nos camps dès maintenant!</p>





                                        <p class="rdv-tel">inscription par téléphone <br><span>418-687-1250</span></p>

                                        <div id="popup_name" class="popup_block">
                                            <h2>Merci de remplir le formulaire d'informations pour le Camp d'été.</h2>
                                            <?php echo do_shortcode('[contact-form-7 id="495" title="Formulaire informations pour Camp"]'); ?>

                                        </div>


                                    </div>

                                <?php } else { ?>


                                    <!-- section pour l'affichage des cours -->



                                    <div class="col-sm-3">

                                        <p class="titre-infos-camp">Descriptif</p>
                                        <p><?php the_field('desc_min'); ?></p>

                                        <p class="titre-infos-camp">Objectifs du cours</p>
                                        <p><?php the_field('obj'); ?></p>
                                    </div>
                                    <div class="col-sm-3">
                                        <!--p class="titre-infos-camp">Pourquoi nous?</p>
                                        <p><?php the_field('pourquoi'); ?></p-->

                                        <p class="titre-infos-camp">Horaire</p>
                                        <p><?php the_field('horaire'); ?></p>


                                    </div>
                                    <div class="col-sm-3 col-droite-camps">

                                        <p class="titre-infos-camp">Tarifs</p>
                                        <p><?php the_field('tarifs'); ?></p>


                                        <p class="titre-infos-camp">Notes</p>
                                        <p><?php the_field('notes'); ?></p>
                                    </div>
                                    <div class="col-sm-3 bloc_inscr">
                                        <!--selection des cours junior pour l'affichage du popup d'inscription -->

                                        <p class="slogan">Inscrivez-vous à nos cours dès maintenant!</p>


                                        <p class="rdv-tel">inscription par téléphone <br><span>418-687-1250</span></p>

                                        <div id="popup_name_cours" class="popup_block">
                                            <h2>Merci de remplir le formulaire d'informations pour le cours junior.</h2>
                                            <?php echo do_shortcode('[contact-form-7 id="583" title="Formulaire informations pour cours junior"]'); ?>

                                        </div>

                                    </div>

                                    </div>

                                <?php }  ?>

                            </div>


                    </div>

                </div>

                <section class="pub_cours">
                    <div class="container">
                        <img src="<?php bloginfo('template_url'); ?>/images/Pro-Sport_logo.png" alt=""/>
                        <div>
                            <p>Vous avez besoin d'équipement?</p>
                            <!--p>Inscrivez-vous et obtenez -20% sur vos achats en ligne</p-->
                        </div>
                        <a href="http://prosportqc.com/" class="btn theme moyen" target="_blank">Magasiner</a>
                    </div>
                </section>

                <?php endwhile; ?>

            </div>

        </div>

    </section>

<?php get_sidebar( "infolettre" ); ?>

<?php get_footer(); ?>