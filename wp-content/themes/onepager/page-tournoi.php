<?php
/*
Template Name: tournoi
*/


get_header('section');


?>

    <section class="" style="background-color: #fff">

        <div class="fond_blanc" >


                 <!-- <div class="row no-margin">

                    <div class="col-xs-12 col-sm-4 no-padding">

                        <img src="<?php bloginfo('template_url'); ?>/images/pub_desjardins.jpg" alt=""/>

                    </div>

                    <div class="col-xs-12 col-sm-4 no-padding">

                        <img src="<?php bloginfo('template_url'); ?>/images/pub_acces-conseil.jpg" alt=""/>

                    </div>

                    <div class="col-xs-12 col-sm-4 no-padding">

                        <img src="<?php bloginfo('template_url'); ?>/images/pub_centre-neilson.jpg" alt=""/>

                    </div>

                </div> -->

        </div>

        <div class="container-fluid fond_blanc content"  id="base">

                <div class="row no-margin">
                    <div class="col-lg-1"></div>
                    <?php

                        // check if the repeater field has rows of data
                        if (have_rows('partenaires')):

                             // loop through the rows of data
                            while (have_rows('partenaires')) : the_row();// display a sub field value?>
								<?php $link = get_sub_field('lien_du_partenaires'); ?>
								<?php if ($link == ''): ?>
                                <div class="col-xs-12 col-lg-2 no-padding">
                                    <img src="<?php the_sub_field('image');?>" />
                                </div>
								<?php else: ?>
								<div class="col-xs-12 col-lg-2 no-padding">
                                     <a href="<?= $link ?>" target='blank'><img src="<?php the_sub_field('image');?>" /></a>
                                </div>
								<?php endif; ?>
                                <?php
                            endwhile;

                        else :

                            // no rows found

                        endif;

                        ?>

					<!-- 	LISTE NINJA BRACKET		 -->

					 <!--<div class="col-xs-12  col-sm-2   categories no-padding">
                        <li><a href="#">Tableaux</a>
                            <ul>
                                <li>
                                    <a href="https://ecolejmg.com/simples-masculins-aaa/">Simple masculin AAA</a>
                                </li>
						                             
								<li>
                                    <a href="https://ecolejmg.com/simple-masculin-aa">Simple masculin AA</a>
                                </li>
                                <li>
                                    <a href="https://ecolejmg.com/simple-masculin-a/">Simple masculin A</a>
                                </li>
<li>
                                    <a href="https://ecolejmg.com/simple-masculin-b/">Simple masculin B</a>
                                </li>
<li>
                                    <a href="https://ecolejmg.com/simple-masculin-c/">Simple masculin C</a>
                                </li>
                                 <li>
                                    <a href="https://ecolejmg.com/simple-feminin-aa/">Simple féminin AA</a>
                                </li> 
                                <li>
                                    <a href="https://ecolejmg.com/simple-feminin-a/">Simple féminin A</a>
                                </li>
                                <li>
                                    <a href="https://ecolejmg.com/simple-feminin-b/">Simple féminin B</a>
                                </li>
 <li>
                                    <a href="https://ecolejmg.com/simple-feminin-c/">Simple féminin C</a>
                                </li>
                                <li>
                                    <a href="https://ecolejmg.com/double-masculin-aa/">Double masculin AA</a>
                                </li>
                                <li>
                                    <a href="https://ecolejmg.com/double-masculin-a/">Double masculin A </a>
                                </li>
<li>
                                    <a href="https://ecolejmg.com/double-masculin-b/">Double masculin B </a>
                                </li>
<li>
                                    <a href="https://ecolejmg.com/double-masculin-b/">Double masculin C </a>
                                </li>
<li>
                                    <a href="https://ecolejmg.com/double-feminin-aa/">Double féminin AA </a>
                                </li>
<li>
                                    <a href="https://ecolejmg.com/double-feminin-a/">Double féminin A </a>
                                </li>
<li>
                                    <a href="https://ecolejmg.com/double-feminin-b/">Double féminin B </a>
                                </li>
<li>
                                    <a href="https://ecolejmg.com/double-feminin-c/">Double féminin C </a>
                                </li>
                                <li>
                                    <a href="https://ecolejmg.com/double-mixte/">Double mixte A</a>
                                </li> 
								 <li>
                                    <a href="https://ecolejmg.com/double-mixte-b/">Double mixte B</a>
                                </li> 
								<li>
                                    <a href="https://ecolejmg.com/junior-balles-regulieres/">Junior balles régulières</a>
                                </li>
								<li>
                                    <a href="https://ecolejmg.com/junior-balles-pt-vert/">Junior balles pt. vert</a>
                                </li>
								<li>
                                    <a href="https://ecolejmg.com/consolations/">Consolations</a>
                                </li>-->
						
                            </ul>
                        </li>
						 
						 
						 <!-- 	FIN LISTE NINJA BRACKET		 -->

                    </div>
                    <div class="col-xs-12 col-sm-7">
                        <!--<a class="btn theme jaune" href="#" id="lk_horaire">Horaire</a>-->
                        <a class="btn theme jaune" href="#" id="lk_photos">Photos</a>
                        <!--<a class="btn theme jaune" href="#" id="lk_aaa">Présentation des joueurs</a>-->
                        <!--<a class="btn theme jaune" href="#" id="lk_actu">Activités de la semaine</a>-->
<!-- 						<a class="btn theme jaune" href="<?= get_home_url() ?>/inscriptions-tournoi" id="lk_actu">Inscriptions</a> -->

                        <p class="titre-camps">Présentation</p>
                        <p class="text"><?php the_content(); ?></p>

                    </div>
                    <div class="col-xs-12 col-sm-2 logo">
                        <img class="logo_cohue" src="<?php bloginfo('template_url'); ?>/images/logo_cohue_bleu.png" alt=""/>
                    </div>



                </div>

            </div>
    </section>

	<section id="photos" class="tournoi-photos">
		<a class="btn theme jaune lk_retour" href="#" >Retour</a>
		<div class="container">
			<div class="row">
				<?php

                $images = get_field('photos_joueurs');

                if ($images): ?>

						<?php foreach ($images as $image): ?>
							<div class="col-sm-6" style='margin-bottom:40px;'>
								<a href="<?php echo $image['sizes']['large']; ?>">
									<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
								</a>
							</div>
						<?php endforeach; ?>

				<?php endif; ?>
			</div>
		</div>
	</section>

<section id="aaa">
    <a class="btn theme jaune lk_retour" href="#" >Retour</a>
    <div class="row no-margin">
        <?php while (have_rows('joueurs_presentation')): the_row();
            // vars
            $categorie = get_sub_field('cat');
            $joueurs = get_sub_field('joueurs');
            ?>


            <?php while (have_rows('joueurs')): the_row();

                $image = get_sub_field('photo_joueur');
                $nom = get_sub_field('nom_joueur');
                $content = get_sub_field('pres_joueur');
                ?>
                <div class="col-xs-12 col-sm-6">
					 <p class="titre-camps"><?php echo $categorie ?></p>
                    <div class="col-xs-12 col-sm-4">
                        <img class="macaron" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                    </div>
                    <div class="col-xs-12 col-sm-8 pres">
                        <p class="titre-camps"> <?php echo $nom; ?></p>
                        <?php echo $content; ?>
                    </div>
                </div>




            <?php endwhile; ?>
            <div class="clearfix"></div>

        <?php endwhile; ?>

    </div>
</section>

<section id="horaires">
    <a class="btn theme jaune lk_retour" href="#" >Retour</a>
    <div class="container-fluid">
	  <div class="col-xs-12 col-sm-6">
        <p class="titre-camps"> Horaires des matchs</p>
		<?php the_field('infos_horaires'); ?>
        <?php while (have_rows('horaires')): the_row();

            // vars
            $date = get_sub_field('date');
            $infos = get_sub_field('infos');

            ?>
		  
            <h3> <?php echo $date ?></h3>


            <div class="date">
                    <table class="tg">
                        <tr>
                            <th class="tg-031e">Heure</th>
							<th class="tg-031e">Nom des joueurs</th>
                            <th class="tg-031e">Catégorie</th>
                            <th class="tg-031e">Terrain</th>
                            <!--<th class="tg-031e">Résultat</th>-->
                        </tr>
			<tr><td class="tg-031e" rowspan="1" style='color:#fff;'>.</td></tr>
            <?php
            $i=0;
            if ($infos): ?>

                <?php while (have_rows('infos')): the_row();

                    $joueur1 = get_sub_field('joueur1');
                    $joueur2 = get_sub_field('joueur2');
                    $cat = get_sub_field('categorie');
                    $heure = get_sub_field('heure');
                    $terrain = get_sub_field('terrain');
                    $resultat = get_sub_field('resultat');?>
                        <tr class="<?php echo ($i%2) ? 'impair top':'pair' ?><?php if ($i>1 && !($i%2)) {
                        echo ' top';
                    }?>">
							<td class="tg-031e" rowspan="1"><?php echo $heure ?></td>
                            <td class="tg-031e"><?php echo $joueur1 ?></td>
                            <td class="tg-031e" rowspan="1"><?php echo $cat ?></td>            
                            <td class="tg-031e" rowspan="1"><?php echo $terrain ?></td>
                            <td class="tg-031e" rowspan="1"><?php echo $resultat ?></td>
							
                        </tr>

                        <tr class="<?php echo ($i%2) ? 'impair':'pair' ?>" style='margin-bottom:10px;'>
							<td class="tg-031e"></td>
                            <td class="tg-031e"><?php echo $joueur2 ?></td>		
                        </tr>
						<tr><td class="tg-031e" rowspan="1" style='color:#fff;'>e</td></tr>
                <?php
                    $i++;
                endwhile; ?>
            <?php endif; ?>
                    </table>

        </div>
        <?php endwhile; ?>
		</div>
    </div>

</section>

    <section id="actu">

        <div class="row no-margin">

            <div class="col-xs-12  col-sm-2    no-padding">
                <a class="btn theme jaune lk_retour" href="#" >Retour</a>
            </div>
            <div class="col-xs-12 col-sm-7">

                <p class="titre-camps"><?php the_field('titre_actu');?></p>
                <div class="text"><?php the_field('texte_actu');?></div>

            </div>
            <div class="col-xs-12 col-sm-2 logo">
                <img class="logo_cohue" src="<?php bloginfo('template_url'); ?>/images/logo_cohue_bleu.png" alt=""/>
            </div>



        </div>

    </section>


<script type="text/javascript">

$("#lk_photos").click(function(e){
        e.preventDefault();
        $("#base").slideUp();

        var $grid = $('.grid').imagesLoaded( function() {
            // init Masonry after all images have loaded
            $grid.masonry({
                itemSelector: '.grid-item'
            });
        });
        $("#photos").slideDown();
        $('html, body').animate({
            scrollTop:($(this).offset().top)-100
        }, 'slow');
    });


    $("#lk_horaire").click(function(e){
        e.preventDefault();
        $("#base").slideUp();

        $("#horaires").slideDown();
        $('html, body').animate({
            scrollTop:($(this).offset().top)-100
        }, 'slow');
    });


    $("#lk_aaa").click(function(e){
        e.preventDefault();
        $("#base").slideUp();
        $("#aaa").slideDown();
        $('html, body').animate({
            scrollTop:($(this).offset().top)-100
        }, 'slow');
    });

    $("#lk_actu").click(function(e){
        e.preventDefault();
        $("#base").slideUp();
        $("#actu").slideDown();
        $('html, body').animate({
            scrollTop:($(this).offset().top)-100
        }, 'slow');
    });

    $(".lk_retour").click(function(){
        $("#photos").hide();
        $("#aaa").hide();
        $("#horaires").hide();
        $("#actu").hide();
        $("#base").slideDown();

        $('html, body').animate({
            scrollTop:($("#base").offset().top)-100
        }, 'slow');

    })

</script>

<?php get_sidebar("infolettre"); ?>

<?php get_footer(); ?>
